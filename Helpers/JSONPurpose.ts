/**
 *  JSONPurpose.ts
 *  Adept
 *  
 *  Often object in Adept are initialised straight off the
 *  JSON sent in with the request/response. Since the content
 *  of JSON varies wildly depending on its source, some way
 *  of tracking the source is required.
 * 
 *  @created    January 1, 2017
 *  @author     Animesh. hey@animesh.xyz
 *  @copyright  Copyright (c) 2016 Green Hill Sustainability
 *              All Rights Reserved
 * 
 */

export enum JSONPurpose {
    /** 
     *  JSON for initially creating an Adept object from client-side input.
     *  GUID and other auto-generated elements are missing
     */
    InitFromClient,
    /**
     *  JSON for instantiaing an Adept object using database response.
     */
    InitFromDb,
    /**
     *  JSON for requesting projections
     */
    Projections,
    /**
     *  JSON for updating an already existing database object.
     */
    Update
}