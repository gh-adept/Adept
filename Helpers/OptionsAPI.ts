/**
 *  OptionsAPI.ts
 *  Adept
 *  
 *  @requires   express
 *  @requires   BKUse, BKDirection, BKDoorMaterial, BKDoorType, BKFloorMaterial,
 *              BKFuel, BKGlazingCavityFill, BKGlazingCavitySpacer, BKGlazingPane,
 *              BKInsulationMaterial, BKLocale, BKWallCavity, BKWallLeaf, BKWallMaterial,
 *              BKWindowFrame 
 * 
 *  @created    November 30, 2016
 *  @author     Animesh. ash@mishra.io
 *  @copyright  Copyright (c) 2016 Green Hill Products Limited.
 *              All Rights Reserved.
 */

import * as Express               from "express"

import { BKUse }                  from "../BuildKit/BKUse"
import { BKControlSystem }        from "../BuildKit/BKControlSystem"
import { BKDirection }            from "../BuildKit/BKDirection"
import { BKDoorMaterial }         from "../BuildKit/BKDoorMaterial"
import { BKDoorType }             from "../BuildKit/BKDoorType"
import { BKFloorMaterial }        from "../BuildKit/BKFloorMaterial"
import * as BKFuel                from "../BuildKit/BKFuel"
import { BKGlazingCavityFill }    from "../BuildKit/BKGlazingCavityFill"
import { BKGlazingCavitySpacer }  from "../BuildKit/BKGlazingCavitySpacer"
import { BKGlazingPane }          from "../BuildKit/BKGlazingPane"
import { BKInsulationMaterial }   from "../BuildKit/BKInsulationMaterial"
import { BKLocale }               from "../BuildKit/BKLocale"
import { BKOccupantActivity }     from "../BuildKit/BKOccupantActivity"
import { BKWallCavity }           from "../BuildKit/BKWallCavity"
import { BKWallLeaf }             from "../BuildKit/BKWallLeaf"
import { BKWallMaterial }         from "../BuildKit/BKWallMaterial"
import { BKWindowFrame }          from "../BuildKit/BKWindowFrame"

/**
 *  Returns options for various building properties and materials.
 *  
 *  @route  /ui/building
 *  @http   GET
 */
export function Building(request: Express.Request, response: Express.Response, next: Express.NextFunction)
{
    response.status(200).json({
        buildingUse         : BKUse.ListTypes(),
        controlSystemTypes  : BKControlSystem.ListTypes(),
        direction           : BKDirection.ListTypes(),
        doorMaterial        : BKDoorMaterial.ListTypes(),
        doorType            : BKDoorType.ListTypes(),
        floorMaterial       : BKFloorMaterial.ListTypes(),
        fuelTypes           : BKFuel.ListTypes(),
        glazingCavityFill   : BKGlazingCavityFill.ListFillTypes(),
        glazingCavitySpacer : BKGlazingCavitySpacer.ListMaterials(),
        glazingPaneCoating  : BKGlazingPane.ListCoatingTypes(),
        insulationMaterial  : BKInsulationMaterial.ListTypes(),
        locale              : BKLocale.ListTypes(),
        occupantActivity    : BKOccupantActivity.ListTypes(),
        wallCavity          : BKWallCavity.ListTypes(),
        wallLeaf            : BKWallLeaf.ListTypes(),
        wallMaterial        : BKWallMaterial.ListTypes(),
        windowFrameMaterial : BKWindowFrame.ListMaterials()
    })
} 