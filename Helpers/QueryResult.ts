/**
 *  QueryResult.ts
 *  Adept
 *  
 *  @created    January 1, 2017
 *  @author     Animesh. hey@animesh.xyz
 *  @copyright  Copyright (c) 2016 Green Hill Products Limited
 *              All Rights Reserved
 *  
 */

export class QueryResult
{
    public docs: Array<any>

    public constructor(docs: Array<any>) {
        this.docs = docs
    }

    public hasDocuments(): boolean
    {
        if(this.docs.length > 0) {
            return true
        }
        else {
            return false
        }
    }

    public summary(): Array<string>
    {
        let documents = []
        for (var doc of this.docs) {
            if(doc.name) {
                documents.push(doc.name)
            }
            else if(doc.firstName) {
                documents.push(doc.firstName + " " + doc.lastName)
            }
        }
        return documents
    }
}