//
//  AdeptError.ts
//  Adept
//
//  Created on November 23, 2016 by Animesh.
//  Copyright (c) 2016 Green Hill Products Limited
//  All Rights Reserved.
//

export enum AdeptErrorType {
    CouldntDelete,
    CouldntEdit,
    CouldntFind,
    CouldntGet,
    CouldntInit,
    Duplicate,
    InvalidParameters,
    WTF
}

export class AdeptError
{
    public readonly adeptErrorType: string
    public message: any

    public constructor(type: AdeptErrorType, message: any) 
    {
        this.adeptErrorType = AdeptError.listTypes()[type]
        this.message = message
    }

    public static listTypes(): Array<string>
    {
        return [
            "Couldn't Delete",
            "Couldn't Edit",
            "Couldn't Find",
            "Couldn't Get",
            "Couldn't Init",
            "Duplicate",
            "Invalid Parameters",
            "WTF"
        ]
    }
}