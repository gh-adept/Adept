/**
 *  ProjectionAPI.ts
 *  Adept.ClimateKit
 * 
 *  @author     Animesh. hey@animesh.xyz
 *  @created    January 10, 2017
 *  @copyright  Copyright (c) 2016 Green Hill Products Limited
 *              All Rights Reserved 
 */

import * as Express from "express"
import * as JWT     from "jsonwebtoken"

import { BKBuilding }       from "../BuildKit/BKBuilding"
import { EKProjection }     from "../EnergyKit/EKProjection"
import { JSONPurpose }      from "./JSONPurpose"

export function EnergyConsumption(request: Express.Request, response: Express.Response, next: Express.NextFunction)
{
    let token = request.headers["x-access-token"]
    let decoded = JWT.decode(token, { complete: true })

    let building = new BKBuilding(JSONPurpose.Projections, request.body, request.body.parent)
    let projections = new EKProjection(building)
    projections.compileData()
    setTimeout(function() {
        response.status(200).json(projections)
    }, 8000)
}