/**
 *  CKProjection.ts
 *  Adept.ClimateKit
 *  
 *  @requires   CKAveraging, CKBatch, CKDataFormat, CKDataset, CKEmissions,
 *              CKLocation, CKLocationType, CKSampling, CKSamplingType, CKPeriod,
 *              CKVariable, CKVariant
 * 
 *  @author     Animesh. hey@animesh.xyz
 *  @copyright  Copyright (c) 2016  Green Hill Products Limited
 *              All Rights Reserved.
 *  
 */

import { CKAveraging }        from "./CKAveraging"
import { CKBatch }            from "./CKBatch"
import { CKDataFormat }       from "./CKDataFormat"
import { CKDataset }          from "./CKDataset"
import { CKEmissions }        from "./CKEmissions"
import { CKLocation }         from "./CKLocation"
import { CKLocationType }     from "./CKLocation"
import { CKSampling }         from "./CKSampling"
import { CKSamplingType }     from "./CKSampling"
import { CKPeriod }           from "./CKPeriod"
import { CKVariable }         from "./CKVariable"
import { CKVariant }          from "./CKVariant"

export enum CKProjectionType {
    Absolute,
    ChangeOnly
}

export class CKProjection
{
    /** Unique ID that represents this projection in the Climate API database */
    public _id        : string
    public averaging  : CKAveraging
    public batch      : CKBatch
    public dataFormat : CKDataFormat = CKDataFormat.Sampled
    public dataset    : CKDataset = CKDataset.Land
    public emissions  : CKEmissions
    public location   : CKLocation
    public period     : CKPeriod
    public sampling   : CKSampling
    public type       : CKProjectionType
    public variable   : CKVariable
    public variants   : Array<CKVariant>

    public constructor(rawData: any, samplingType?: CKSamplingType) 
    {
        this._id        = rawData._id
        this.averaging  = this.parseAveraging(rawData.temporalAveraging)
        this.batch      = this.parseBatch(rawData._id)
        this.emissions  = this.parseEmissions(rawData.emissionsScenario)
        this.location   = new CKLocation(rawData.location.name)
        this.period     = this.parsePeriod(rawData.timePeriod)
        if(samplingType) {
            this.sampling = new CKSampling(samplingType)
        }
        else {
            this.sampling   = new CKSampling(CKSamplingType.ByPercentile)
        }
        this.variable   = this.parseVariable(rawData.variable.name)
        if(rawData.changeOnly) {
            this.type = CKProjectionType.ChangeOnly
        }
        else {
            this.type = CKProjectionType.Absolute
        }
        
        // Sample data
        let samples     = rawData.projections
        this.variants   = this.sampleData(this.sampling, samples)
    }

    private sampleData(sampling: CKSampling, samples: Array<number>): Array<CKVariant>
    {
        // Defaulting to sampling by percentiles
        let variants = this.getAllVariants(samples)
        let sorted   = variants.sort( (a, b) => { return a.value - b.value })
        let variantsByPercentile = []

        sampling.percentiles.forEach( (percentile) => {
            let index = Math.ceil((percentile/100.0) * sorted.length)
            variantsByPercentile.push(new CKVariant(sorted[index].id, sorted[index].value, percentile))
        })

        return variantsByPercentile
    }

    private getAllVariants(samples: Array<number>): Array<CKVariant>
    {
        let variants = []
        for (var i = 0; i < samples.length; i++) {
            variants.push(new CKVariant(i, samples[i]))
        }
        return variants
    }

    private parseAveraging(a: string): CKAveraging
    {
        switch(a.toLowerCase()) {
        case "annual":
            return CKAveraging.Annual
        case "january":
            return CKAveraging.January
        case "february":
            return CKAveraging.February
        case "march":
            return CKAveraging.March
        case "april":
            return CKAveraging.April
        case "may":
            return CKAveraging.May
        case "june":
            return CKAveraging.June
        case "july":
            return CKAveraging.July
        case "august":
            return CKAveraging.August
        case "september":
            return CKAveraging.September
        case "october":
            return CKAveraging.October
        case "november":
            return CKAveraging.November
        case "december":
            return CKAveraging.December
        case "winter (december, january, february)":
            return CKAveraging.Winter
        case "spring (march, april, may)":
            return CKAveraging.Spring
        case "summer (june, july, august)":
            return CKAveraging.Summer
        case "autumn (september, october, november)":
            return CKAveraging.Autumn
        default:
            return CKAveraging.Annual
        }
    }

    private parseBatch(id: String): CKBatch
    {
        if(id.startsWith("bat1")) {
            return CKBatch.Batch1
        }
        else if(id.startsWith("bat2")) {
            return CKBatch.Batch2
        }
        else {
            return CKBatch.None
        }
    }

    private parseEmissions(e: string): CKEmissions
    {
        switch(e.toLowerCase()) {
        case "high (a1fi)":
            return CKEmissions.High
        case "low (b1)":
            return CKEmissions.Low
        case "medium (a1b)":
            return CKEmissions.Medium
        default:
            return CKEmissions.High
        }
    }

    private parsePeriod(p: string): CKPeriod
    {
        switch(p.toLowerCase()) {
        case "2010-2039":
            return CKPeriod.c2020
        case "2020-2049":
            return CKPeriod.c2030
        case "2030-2059":
            return CKPeriod.c2040
        case "2040-2069":
            return CKPeriod.c2050
        case "2050-2079":
            return CKPeriod.c2060
        case "2060-2089":
            return CKPeriod.c2070
        case "2070-2099":
            return CKPeriod.c2080
        }
    }

    private parseVariable(v: string): CKVariable
    {
        switch(v.toLowerCase()) {
        case "mean temperature":
            return CKVariable.MeanDailyTemp
        case "maximum temperature":
            return CKVariable.MeanDailyMaxTemp
        case "minimum temperature":
            return CKVariable.MeanDailyMinTemp
        case "total cloud cover":
            return CKVariable.TotalCloud
        case "daily precipitation rate":
            return CKVariable.Precipitation
        case "relative humidity":
            return CKVariable.RelativeHumidity
        case "sea level pressure":
            return CKVariable.MeanSeaLevelPressure
        default:
            return CKVariable.MeanDailyTemp
        }
    }
}