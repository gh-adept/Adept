/**
 *  CKAveraging.ts
 *  Adept.ClimateKit
 * 
 *  Defines the various temporal averaging methods 
 *  available for generating a climate data projection.
 *  
 *  @created    December 30, 2016
 *  @author     Animesh. hey@animesh.xyz
 *  @copyright  Copyright (c) 2016 Green Hill Products Limited
 *              All Rights Reserved
 * 
 */

export enum CKAveraging {
    Annual,
    January,
    February,
    March,
    April,
    May,
    June,
    July,
    August,
    September,
    October,
    November,
    December,
    Autumn,
    Spring,
    Summer,
    Winter
}