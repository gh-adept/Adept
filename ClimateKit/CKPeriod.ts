//
//  TimePeriod.ts
//  Climate API
//
//  Created on October 21, 2016 by Animesh.
//  Copyright (c) 2016 Greenhill Sustainability
//  All rights reserved.
//

/**
 *  Time period is represented as follows:
 *  - **2020s**, 2010-2039
 *  - **2030s**, 2020-2049
 *  - **2040s**, 2030-2059
 *  - **2050s**, 2040-2069
 *  - **2060s**, 2050-2079
 *  - **2070s**, 2060-2089
 *  - **2080s**, 2070-2099
 */
export enum CKPeriod 
{
    c2020,
    c2030,
    c2040,
    c2050,
    c2060,
    c2070,
    c2080
}