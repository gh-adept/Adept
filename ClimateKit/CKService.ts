/**
 *  CKService.ts
 *  Adept.ClimateKit
 *  
 *  @requires   request
 *  @requires   CKProjection, CKRequest
 * 
 *  @created    December 30, 2016
 *  @author     Animesh. hey@animesh.xyz
 *  @copyright  Copyright (c) 2016 Green Hill Products Limited
 *              All Rights Reserved
 *  
 */

import * as Request from "request"
import { CKProjection } from "./CKProjection"
import { CKRequest } from "./CKRequest"

export class CKService
{
    public static Query(request: CKRequest, callback: (projections?: Array<CKProjection>) => void)
    {
        let queryURL = "https://climate.eu-gb.mybluemix.net/api/v1/query"

        // Query parameters
        let predicate = {
            selector: {
                "location.name": request.location,
                "variable.name": request.variable,
                "timePeriod": request.period,
                "temporalAveraging": request.averaging
            }
        }

        Request(queryURL, { method: "POST", json: predicate }, (error, response, body) => {
            if (error) {
                callback(null)
            }
            else {
                if (Array.isArray(body)) {
                    let projections = []
                    for (var doc of body) {
                        projections.push(new CKProjection(doc))
                    }
                    callback(projections)
                }
                else {
                    console.log(body)
                    callback(null)
                }
            }
        })
    }

    public static GetAll(location: string, variable: string, callback: (projections?: Array<CKProjection>) => void)
    {
        let queryURL = "https://climate.eu-gb.mybluemix.net/api/v1/query"

        let predicate = {
            selector: {
                "location.name": location,
                "variable.name": variable
            }
        }

        Request(queryURL, { method: "POST", json: predicate }, (error, response, body) => {
            if (error) {
                callback(null)
            }
            else {
                if (body.length > 0) {
                    let projections = []
                    for (var doc of body) {
                        projections.push(new CKProjection(doc))
                    }
                    callback(projections)
                }
                else {
                    console.log(body)
                    callback(null)
                }
            }
        })
    }
}