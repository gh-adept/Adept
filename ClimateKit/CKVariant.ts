//
//  Variant.ts
//  Climate API
//
//  Created on October 21, 2016 by Animesh.
//  Copyright (c) 2016 Greenhill Sustainability
//  All rights reserved.
//

/**
 *  UKCP09 data models outputs 10,000 predictions for each request for
 *  a given climate variable, time period, emissions scenario and location.
 *  These predictions are termed "variants". Clearly, one has to come up with
 *  a way to sample a subset of these 10,000 variants to do anything meaningful
 *  with them. Class *Sampling* defines how that can be done
 */
export class CKVariant 
{
    /** The variant ID is a number from 0-9999 unique for variants in the same request.*/
    public id: number

    /** The predicted value of the climate variable */
    public value: number
    
    /** If the variant was sampled by percentile, this property identifies what
     *  percentile this variant represents. Otherwise, it's set to -1.
     */
    public percentile: number 

    public constructor(id: number, value: number, percentile?: number) 
    {
        this.id = id
        this.value = value
        this.percentile = percentile
    }
}