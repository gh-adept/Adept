/**
 *  CKEmissions.ts
 *  Adept.ClimateKit
 * 
 *  A plausible representation of the future development of emissions
 *  of substances (e.g. greenhouse gases and aerosols) that can affect
 *  the radiative balance of the globe. These representations are based
 *  on a coherent and internally consistent set of assumptions about
 *  determining factors (such as demographic and socio-economic
 *  development, technological change etc.) and their key relationships.
 *  
 *  The emissions scenarios used in Green Hill's Climate API do not 
 *  include the effects of planned mitigation policies, but do assume
 *  different pathways of technological and economic growth which include
 *  a switch from fossil fuels to renewable sources of energy.
 * 
 *  The emissions scenarios are defined as per the IPCC SRES standard
 *  as follows:
 *  - Low, B1
 *  - Medium, A1B
 *  - High, A1FI 
 *  
 *  @created    December 30, 2016
 *  @author     Animesh. hey@animesh.xyz
 *  @copyright  Copyright (c) 2016 Green Hill Products Limited
 *              All Rights Reserved
 * 
 */

export enum CKEmissions {
    High,
    Low,
    Medium
}