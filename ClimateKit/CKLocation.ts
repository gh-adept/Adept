/**
 *  CKLocation.ts
 *  Adept.ClimateKit
 *  
 *  The geographic place of interest for which a projection is
 *  provided.
 * 
 *  @created    October 20, 2016
 *  @author     Animesh. hey@animesh.xyz
 *  @copyright  Copyright (c) 2016 Green Hill Products Limited
 *              All Rights Reserved
 * 
 */

/**
 *  A location can be of type GridSquare, Marine or an aggregated
 *  region. 
 */
export enum CKLocationType 
{
    GridSquare,
    Marine,
    Region
}

export class CKLocation 
{
    public id: string
    public type: CKLocationType
   
    public constructor(id: string, type?: CKLocationType) 
    {
        this.id = id
        if(type) {
            this.type = type
        }
        else {
            this.type = CKLocationType.GridSquare
        }
    }
}