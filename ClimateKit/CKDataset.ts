/**
 *  CKDataset.ts
 *  Adept.ClimateKit
 * 
 *  Defines the dataset that contains the given projection
 *  
 *  @created    December 30, 2016
 *  @author     Animesh. hey@animesh.xyz
 *  @copyright  Copyright (c) 2016 Green Hill Products Limited
 *              All Rights Reserved
 * 
 */

export enum CKDataset
{
    Land,
    Marine,
    SeaLevelRise,
    StormSurge
}