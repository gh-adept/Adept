//
//  CKSampling.ts
//  Adept.ClimateKit
//
//  Created on October 21, 2016 by Animesh.
//  Copyright (c) 2016 Greenhill Sustainability
//  All rights reserved.
//

/**
 *  The sampling method employed to pick a subset from the 10,000 odd
 *  variants in a given climate variable projection.
 */
export enum CKSamplingType
{
    SelectAll,
    Random,
    ByID,
    ByPercentile
}

export class CKSampling 
{
    public type: CKSamplingType

    // Number of random samples. Applicable only when sampling method
    // is 'random'
    public count: number

    // IDs of the variants to be sampled, must be between 0-9999
    public IDs: Array<number>

    // Percentiles the data was sampled at, only applicable when
    // sampling method is 'by percentile'
    public percentiles: Array<number>

    public constructor(type: CKSamplingType, percentiles?: Array<number>, count?: number) 
    {
        this.type = type 
        if (type == CKSamplingType.ByPercentile) {
            this.count = 5
            if (percentiles != undefined) {
                this.percentiles = percentiles
            }
            else {
                this.percentiles = [10, 33, 50, 66, 90]
            }
        }
        else {
            this.count = count
        }
    }
}