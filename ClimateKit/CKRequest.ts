/**
 *  CKRequest.ts
 *  Adept.ClimateKit
 * 
 *  @created    December 30, 2016
 *  @author     Animesh. hey@animesh.xyz
 *  @copyright  Copyright (c) 2016 Green Hill Products Limited
 *              All Rights Reserved
 */

import { CKAveraging }      from "./CKAveraging"
import { CKEmissions }      from "./CKEmissions"
import { CKLocation }       from "./CKLocation"
import { CKLocationType }   from "./CKLocation"
import { CKPeriod }         from "./CKPeriod"
import { CKVariable }       from "./CKVariable"

const baseURL = "https://climate.eu-gb.mybluemix.net/api/v1"

export class CKRequest
{
    public averaging : string
    public emissions : string
    public location  : string
    public period    : string
    public variable  : string

    public constructor(v: CKVariable, l: CKLocation, a?: CKAveraging, e?: CKEmissions, p?: CKPeriod, )
    {
        this.averaging = this.parseAveraging(a)
        this.emissions = this.parseEmissions(e)
        this.location = `${l.id}`
        this.period = this.parsePeriod(p)
        this.variable = this.parseVariable(v)
    }

    private parseAveraging(averaging: CKAveraging): string
    {
        switch(averaging) {
        case CKAveraging.Annual:
            return "Annual"
        case CKAveraging.January:
            return "January"
        case CKAveraging.February:
            return "February"
        case CKAveraging.March:
            return "March"
        case CKAveraging.April:
            return "April"
        case CKAveraging.May:
            return "May"
        case CKAveraging.June:
            return "June"
        case CKAveraging.July:
            return "July"
        case CKAveraging.August:
            return "August"
        case CKAveraging.September:
            return "September"
        case CKAveraging.October:
            return "October"
        case CKAveraging.November:
            return "November"
        case CKAveraging.December:
            return "December"
        case CKAveraging.Autumn:
            return "Autumn (September, October, November)"
        case CKAveraging.Spring:
            return "Spring (March, April, May)"
        case CKAveraging.Summer:
            return "Summer (June, July, August)"
        case CKAveraging.Winter:
            return "Winter (December, January, February)"
        default:
            return "Summer (June, July, August)"
        }
    }

    private parseEmissions(emissions: CKEmissions): string
    {
        switch(emissions) {
        case CKEmissions.High:
            return "High (A1FI)"
        case CKEmissions.Low:
            return "Low (B1)"
        case CKEmissions.Medium:
            return "Medium (A1B)"
        default:
            return "High (A1FI)"
        }
    }

    private parsePeriod(period: CKPeriod): string
    {
        switch(period) {
        case CKPeriod.c2020:
            return "2010-2039"
        case CKPeriod.c2030:
            return "2020-2049"
        case CKPeriod.c2040:
            return "2030-2059"
        case CKPeriod.c2050:
            return "2040-2069"
        case CKPeriod.c2060:
            return "2050-2079"
        case CKPeriod.c2070:
            return "2060-2089"
        case CKPeriod.c2080:
            return "2070-2099"
        default:
            return "2020-2049"
        }
    }

    private parseVariable(variable: CKVariable): string
    {
        switch(variable) {
        case CKVariable.MeanDailyTemp:
            return "Mean Temperature"
        case CKVariable.MeanDailyMaxTemp:
            return "Maximum Temperature"
        case CKVariable.MeanDailyMinTemp:
            return "Minimum Temperature"
        case CKVariable.MeanSeaLevelPressure:
            return "Sea Level Pressure"
        case CKVariable.Precipitation:
            return "Daily Precipitation Rate"
        case CKVariable.RelativeHumidity:
            return "Relative Humidity"
        case CKVariable.TotalCloud:
            return "Total Cloud Cover"
        default:
            return "Maximum Temperature"
        }
    }
}