/**
 *  CKDataFormat.ts
 *  Adept.ClimateKit
 *  
 *  @created    December 30, 2016
 *  @author     Animesh. hey@animesh.xyz
 *  @copyright  Copyright (c) 2016 Green Hill Products Limited
 *              All Rights Reserved
 * 
 */

export enum CKDataFormat {
    CDF,
    PDF,
    Sampled
}