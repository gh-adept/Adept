/**
 *  CKBatch.ts
 *  Adept.ClimateKit
 * 
 *  A batch represents a group of variables that, within climate models,
 *  have been processed together for each location.
 *  
 *  Some users wish to examine joint probabilities between multiple variables.
 *  The method that generates the sampled data incorporates a multivariate
 *  analysis that can only process a certain number of variables simultaneously
 *  This limitation means that the variables have been processed in two
 *  separate **batches**. Examining joint probabilities between variables
 *  in different batches is not recommended.
 *  
 *  @created    December 30, 2016
 *  @author     Animesh. hey@animesh.xyz
 *  @copyright  Copyright (c) 2016 Green Hill Products Limited
 *              All Rights Reserved
 * 
 */

export enum CKBatch
{
    None,
    Batch1,
    Batch2
}
