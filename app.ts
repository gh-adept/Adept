/**
 *  app.ts
 *  Adept
 * 
 *  Configures and runs the Adept API server.
 *  
 *  @requires   express, dotenv, cfenv
 *  @requires   AppInfo, DataService, Router 
 * 
 *  @created    November 19, 2016
 *  @author     Animesh. ash@mishra.io
 *  @copyright  Copyright (c) 2016 Green Hill Products Limited.
 *              All Rights Reserved.
 */

import * as Express         from "express"
import * as DotEnv          from "dotenv"
import * as CloudFoundry    from "cfenv"

import { AppInfo }          from "./Controllers/AppInfo"
import { DataService }      from "./Controllers/DataService"
import { Router }           from "./Controllers/Router"

/**
 *  Grab deployment information from Cloud Foundry environment
 *  and initialize an `AppInfo` instance.
 */
let appEnvironment = CloudFoundry.getAppEnv()
let appInfo = new AppInfo(appEnvironment.bind, appEnvironment.port, appEnvironment.url)

/**
 *  Get database access credentials from Cloud Foundry environment.
 *  If debugging locally, read it from a local `.env` file that's not
 *  checked into source control.
 */
let dbCredentials = appEnvironment.getServiceCreds("Adept DB")
let dbURL: string
if (dbCredentials == null) {
    console.log("Reading local credentials...")
    DotEnv.config()
    dbURL = process.env.dbURL
}
else {
    dbURL = dbCredentials.url
}

/**
 *  Now that we have database credentials, let's try to initialise the data service.
 *  If successful, create an Express application and configure its router using 
 *  `AppInfo` and `DataService` initialised here. Otherwise, terminate the application
 *  and print the error to the console.
 */
let dataService = new DataService(dbURL, error => {
    if (error) {
        console.log("Failed to initialize database service. Here's what I know:")
        console.log(error.message)
    }
    else {
        let app = Express()
        Router.Configure(app, appInfo, dataService)
        app.listen(appInfo.port, appInfo.hostname, function() {
            console.log(`Server running at ${appInfo.url}`)
        })
    }
})