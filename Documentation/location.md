# Location

It's imperative that we know the exact location (latitude & longitude) of the building. Location is needed to pull
up-to-date weather data for our models. Our climate projection models work a little differently. Projections are 
provided for a given square in this [grid](https://adept-demo.eu-gb.mybluemix.net/grid.png), which is why we also
need to store the `type` and `id` of location accordingly.

The `Location` initializer expects all the properties listed below.

## Properties

|                           |               |                                                                               |
| ------------------------- | ------------- | ----------------------------------------------------------------------------- |
| type                      | `string`      | Required for climate projections. Can be: "grid square" or "region"           |
| id                        | `string`      | Depending on the type, this is either the grid square number or region name. **The grid square number must be sent as a `string` and not a `number`.**  |
| latitude                  | `string`      |                                                                               |
| longitude                 | `number`      |                                                                               |

## Error Handling

```
locationError = {
    "type" = "CouldntInit"
    "message" = "latitude not provided."
}
```

`LocationError.Type` can only have one type: "CouldntInit".