# Authentication

Tokens are the best way to handle authentication for multiple users (as is the case with APIs). The main upsides of 
tokens are:

* Stateless and scalable servers
* Mobile application ready
* Pass authentication to other applications
* Extra security

## How it works

![authentication](assets/tokens.png)

No session information means your application can scale and add more machines as necessary without worrying about 
whether a user is logged in. Although implementation can wary, the gist of it is as follows:

1. User requests access with username/password
2. Application validates credentials
3. Application provides a signed token to the client
4. Client stores that token and sends it along with every request
5. Server verifies token and responds with data

Every single request will require the token. This token should be sent in the HTTP header so that we keep with the 
idea of stateless HTTP requests. We could even create a permission based token and pass this along to a third-party 
application (say a new mobile app we want to use) and they will be able to have access to our data - but only the 
information that we allowed with that specific token.

## Benefits of token

### Stateless and scalability
Tokens are stored on client side. Completely stateless and ready to be scaled. Our load balancers are able to pass 
a user along to any of our server instances since there is no state or session information anywhere.

If were to keep session information on a user that was logged in, this would require us to keep sending that user 
to the same server that they logged in at (called session affinity). This brings problems since, some users would 
be forced to the same server and this could bring about a spot of heavy traffic.

### Security
The token, not a cookie, is sent on every request and since there is no cookie being sent, this helps to prevent 
CSRF attacks. There is no session based information to manipulate since we don’t have a session.

The token also expires after a set amount of time, so a user will be required to login once again. This helps us stay 
secure. 

### Extensibility
Tokens will allow us to build applications that share permissions with another. We can provide selective permissions 
to third-party applications.

## Sign up
Signup page is live [here](https://www.ghs4.uk/register.html).

```
POST https://www.ghs4.uk/signup

REQUEST BODY
{
    "firstName": "Animesh",
    "lastName": "Mishra",
    "organisation": "Animesh Ltd",
    "city": "Birmingham",
    "email": "ash@mishra.io",
    "password": "abcdefghijkl"
}
```

Upon success, the server returns the following:
```
RESPONSE BODY
{ 
    "ok": true
}
```

You must immediately sign in after a successful signup to retrieve the access token.

## Log in
Login page is live [here](https://www.ghs4.uk/login.html).

```
POST https://www.ghs4.uk/authenticate

REQUEST BODY
{
    "email": "ash@mishra.io",
    "password": "abcdefghijkl"
}
```

Upon success, the server returns the following:
```
RESPONSE BODY
{
    "_id": "1b3aec4775edcb3548d056c42a22640e",
    "firstName": "Animesh",
    "lastName": "Mishra",
    "email": "ash@mishra.io",
    "organisation": "Animesh Ltd",
    "city":"Birmingham"
}

RESPONSE HTTP HEADERS
x-access-token  slkfjslkfjiosenflksdf.sdfksnlfekfnskfnlwekfns.sdfkjsekfbwkjesifjwoelsekjf
```

The client should save the `_id` to request data for this particular user. You must supply the
token provided with every API call (except /signup and /authenticate), otherwise the server will
return a `HTTP 403 Forbidden` error. The token is user-specific and expires every 7 days. To 
get a new token, the client will have to sign in again (which can be done silently).

### Log out
Since the server is stateless, to log out a user you merely have to delete the token stored 
client-side.