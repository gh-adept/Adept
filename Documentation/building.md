# Building

Most of the math involved in projections is concerned more about the volume of a building
than its surface area. Hence, Adept conceptualizes a building to be a cuboid that circumscribes
a building of given length, breadth and height (**all lengths are stored in millimetres.**). 
For complex building plans later on, we may break the plan into constituent cuboidal shapes to 
ensure consistency.

![Building model illustration](assets/building.jpg)

The four lateral faces of the cuboid are the outside [walls](walls.md), the top face is the [roof](roof.md) 
and the bottom is the ground [floor](floor.md). Each wall has its own set of [windows](window.md), 
[doors](door.md) and insulation. Likewise, the roof and the floor come with their
respective insulation properties too. Since internal floors, walls, doors and windows don't have 
any effect on a building's interaction with the outside environment, they are not included in the model.

Occupancy information such as building's use, opening/closing hours, heating fuel type and occupants'
activities inform the baseline energy use estimates.

## Properties

|                           |               |                                                                               |
| ------------------------- | ------------- | ----------------------------------------------------------------------------- |
| name                      | `string`      |                                                                               |
| yearBuilt                 | `number`      | Year in which the building was built                                          |
| parent                    | `string`      | ID of the `User` account which owns this building                             |
| locale                    | `Locale`      | See [Location](location.md)                                                   |
| length                    | `number`      | In millimetres. Can be fractional (expressed in decimals)                     |
| breadth                   | `number`      | In millimetres. Can be fractional                                             |
| storeyHeight              | `number`      | Floor to ceiling height in millimetres. Can be fractional                     |
| storeys                   | `number`      | Number of storeys. Can't be fractional                                        |
| walls                     | `Array<Wall>` | See [Wall](wall.md)                                                           |
| roof                      | `Roof`        | See [Roof](roof.md)                                                           |
| floor                     | `Floor`       | See [Floor](floor.md)                                                         |
| use                       | `BuildingUse` | See [BuildingUse](buildinguse.md)                                             |
| heatingFuel               | `string`      | Fuel used for the heating system. Must be one of: "natural gas", "oil", "coal", "grid electricity", "renewable electricity", "lpg", "biogas" |


---

## `POST` Create a new building
A new building must be created immediately after the user has provided the mandatory information - `name`, 
`yearBuilt`, `location`, `length`, `width`, `storeyHeight` and `storeys`. This helps ensure the building is
created securely on the backend (or if a similar building exists, the full building model is sent to the 
frontend for user confirmation).

Further information to the building model such as a `Wall` or a `Roof` can be added later by making an update
request. See **Updating a building** below for more details.

### Route
`https://www.ghs4.uk/api/v1/buildings`

### Parameters
```
X-Access-Token

{
    "name": "some name",
    "yearBuilt": 1993,
    "locale": {
        "longitude": 12.382,
        "latitude": 46.981,
        "type": "Grid Box"
        "id": 1429
    }
    "length": 190.21,
    "width": 124.00,
    "storeyHeight": 3000,
    "storeys": 10
}
```

### Response

**Success** `HTTP 200`         
Returns a newly minted Building object initialised with the given parameters.

**Failure** `HTTP 500`                
Errors from the database engine are encapsulated as an `AdeptError` of type `CouldntInit`
with the `message` property set to the response from database. All other errors are sent 
as-is as a JSON response. 

---

## `GET` Retrieve a building

### Route
`https://www.ghs4.uk/api/v1/buildings/<buildingID>`

### Parameters
`X-Access-Token`

### Response
**Success** `HTTP 200`         
Returns a building with given ID.

**Failure**                 
`HTTP 403`      
If the requested building's parent (i.e. the user account that created the building) doesn't
match the user whose access token is supplied with the request, a `CouldntGet` error is 
thrown with an appropriate `message`.

`HTTP 500`      
Other server errors are sent as-is as a JSON response.

---

## `GET` Retrieve all buildings
Returns all buildings belonging to the user identified by the access token.

### Route
`https://www.ghs4.uk/api/v1/buildings/`

### Parameters
`X-Access-Token`

### Response
**Success** `HTTP 200`         
Returns all building models the user has authored.

**Failure** `HTTP 500`                
Errors from the database engine are encapsulated as an `AdeptError` of type `CouldntGet`
with the `message` property set appropriately.

---

## `PUT` Modify a building
Please note that this is not a patch/update operation. The object in the database will be
**replaced** with the one provided in the request body.

### Route
`https://www.ghs4.uk/api/v1/buildings/<buildingID>`

### Parameters
```
X-Access-Token

REQUEST BODY
Full building object with modified properties to replace the existing object.
```

### Response
**Success** `HTTP 200`         
Returns a `{ "ok": true }` JSON response.

**Failure**                 
`HTTP 403`      
If the requested building's parent (i.e. the user account that created the building) doesn't
match the user whose access token is supplied with the request, a `CouldntEdit` error is 
thrown with an appropriate `message`.

`HTTP 500`      
Other server errors are sent as-is as a JSON response.

---

## `DELETE` Delete a building

### Route
`https://www.ghs4.uk/api/v1/buildings/<buildingID>`

### Parameters
`X-Access-Token`

### Response
**Success** `HTTP 200`         
Returns a confirmation message as response body.

**Failure**                 
`HTTP 403`      
If the requested building's parent (i.e. the user account that created the building) doesn't
match the user whose access token is supplied with the request, a `CouldntDelete` error is 
thrown with an appropriate `message`.

`HTTP 500`      
Other server errors are sent as-is as a JSON response.

---

## The Building Object
```
{
    "parent": "f6333fd7bab72e3d0f79905a971c7cae",
    "name": "Building Tencents",
    "yearBuilt": 1223,
    "length": 190.21,
    "width": 124,
    "storeys": 10,
    "storeyHeight": 3000,
    "locale": {
      "type": "Grid Box",
      "id": "1429",
      "latitude": 46.981,
      "longitude": 12.382
    },
    "_id": "73fe42366f60c466ec6882f8b1531082",
    "_rev": "3-14c4422ccff7d1cc47d9da41bdee4971",
    "walls": [
      {
        "orientation": "west",
        "cavity": {
          "type": "fully filled",
          "width": 200,
          "insulation": {
            "thickness": 200,
            "material": "phenolic foam",
            "rValue": 10
          },
          "rValue": 10
        },
        "leaves": [
          {
            "type": "inner",
            "thickness": 20,
            "material": "Gypsum - High Density",
            "insulation": {
              "thickness": 20,
              "material": "hemp",
              "rValue": 0.5
            },
            "rValue": 0.5465116279069767
          },
          {
            "type": "outer",
            "thickness": 40,
            "material": "Concrete - Reinforced",
            "insulation": {
              "thickness": 40,
              "material": "aerogel",
              "rValue": 2.857142857142857
            },
            "rValue": 2.874534161490683
          }
        ],
        "windows": [
          {
            "glazing": {
              "width": 30,
              "panes": [
                {
                  "thickness": 12,
                  "rValue": 0.021,
                  "coating": "Unknown"
                },
                {
                  "thickness": 4,
                  "rValue": 0.021,
                  "coating": "Unknown"
                }
              ],
              "cavities": [
                {
                  "width": 20,
                  "fill": "argon",
                  "spacerMaterial": "Aluminium",
                  "rValue": 1.2500975609756098
                },
                {
                  "width": 20,
                  "fill": "argon",
                  "spacerMaterial": "Aluminium",
                  "rValue": 1.2500975609756098
                }
              ],
              "rValue": 2.5421951219512193
            },
            "frame": {
              "thickness": 20,
              "material": "Aluminium",
              "rValue": 0.00044444444444444447
            },
            "rValue": 2.5426395663956636
          },
          {
            "glazing": {
              "width": 20,
              "panes": [
                {
                  "thickness": 12,
                  "rValue": 0.133,
                  "coating": "Unknown"
                },
                {
                  "thickness": 16,
                  "rValue": 0.432,
                  "coating": "Unknown"
                }
              ],
              "cavities": [
                {
                  "width": 20,
                  "fill": "argon",
                  "spacerMaterial": "Aluminium",
                  "rValue": 1.2500975609756098
                },
                {
                  "width": 20,
                  "fill": "argon",
                  "spacerMaterial": "Aluminium",
                  "rValue": 1.2500975609756098
                }
              ],
              "rValue": 3.0651951219512195
            },
            "frame": {
              "thickness": 90,
              "material": "PVC",
              "rValue": 0.5625
            },
            "rValue": 3.6276951219512195
          }
        ],
        "doors": [
          {
            "type": "accessible",
            "height": 120,
            "width": 200,
            "thickness": 20,
            "material": "Softwood",
            "rValue": 0.14285714285714285
          },
          {
            "type": "standard",
            "height": 220,
            "width": 100,
            "thickness": 40,
            "material": "Steel",
            "rValue": 0.0008888888888888889
          }
        ],
        "rValue": 13.42104578939766
      }
    ],
    "fuels": [
      {
        "use": "Heating",
        "type": "Coal"
      },
      {
        "use": "Cooling",
        "type": "Natural Gas"
      }
    ],
    "roof": {
      "insulation": {
        "thickness": 100,
        "material": "Extruded Polystyrene (XPS)",
        "rValue": 2.857142857142857
      },
      "rValue": 2.857142857142857
    },
    "floor": {
      "type": "Solid",
      "thickness": 200,
      "insulation": {
        "thickness": 50,
        "material": "Cellulose",
        "rValue": 1.3157894736842106
      },
      "material": "Timber - Hardwood",
      "rValue": 2.426900584795322
    },
    "use": {
      "workdaysPerWeek": 5,
      "workweeksPerYear": 50,
      "type": "Library",
      "minTemp": 15,
      "maxTemp": 23,
      "occupancy": {
        "averageBodyArea": 1.8,
        "count": 500,
        "activity": {
          "type": "Light Machine Work",
          "heatGeneration": 116.4
        },
        "totalHeatGeneration": 104760
      },
      "opensAt": 830,
      "closesAt": 2230,
      "hoursPerDay": 14
    }
}
```