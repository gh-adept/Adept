# Window Frame

![Window Frame illustration](assets/window-frame.png)

## Properties
|                           |                           |                                                               |
| ------------------------- | ------------------------- | ------------------------------------------------------------- |
| material                  | `string`                  | Can be: "softwood", "hardwood", "upvc", "aluminium", "steel"  |
| width                     | `number`                  | In millimetres                                                |
| thickness                 | `number`                  | In millimetres                                                |

## Window Frame Object
```
{
    material: "hardwood",
    width: 28,
    thickness: 300
}
```