# Window

The building model stores windows on a per-wall basis. Windows are broken down into two parts - glazing and frame.

![Window Model](assets/window.png)

## Properties
|                           |                   |                                                                               |
| ------------------------- | ----------------- | ----------------------------------------------------------------------------- |
| glazing                   | `WindowGlazing`   | See [Window Glazing](window-glazing.md)                                       |
| frame                     | `WindowFrame`     | See [Window Frame](window-frame.md)                                           |

## The Window Object
```
{
    "frame": {
        "material": ???,
        "thickness": 20
    },
    "glazing": {
        "length": 15,
        "width": 30,
        "panes": [
            {
                "coating": ???,
                "thickness": 12
            },
            {
                "coating": ???,
                "thickness": 4
            }
        ],
        "cavities": [
            {
                "width": 20,
                "fill": "argon",
                "spacerMaterial": ???
            },
            {
                "width": 20,
                "fill": "argon",
                "spacerMaterial": ???
            }
        ]
    }
}
```