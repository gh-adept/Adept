# User

## Properties

|                           |               ||
| ------------------------- | ------------- ||
| firstName                 | `string`      ||
| lastName                  | `string`      ||
| email                     | `string`      ||
| password                  | `string`      | No password policy is in place. Server doesn't store passwords in plain-text. It hashes user's password using a randomly generated salt, and stores the result in the database.
| organisation              | `string`      ||
| city                      | `string`      ||

## Get user details
```
GET /users/<userID>

REQUEST HEADER
x-access-token      <access token>
```

## Update user details
```
PUT /users/<userID>

REQUEST BODY
{
    "firstName": "Animesh",
    "lastName": "Mishra",
    "organisation": "Green Hill Products Limited",
    "city": "Birmingham",
    "email": "ash@mishra.io"
    "password": <new password, optional>
}

REQUEST HEADER
x-access-token    <access token>
```

## Delete a user
```
DELETE /users/<userID>

REQUEST HEADERS
x-access-token  <access token>
```