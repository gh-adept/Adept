# Organisation

An `Organisation` is a collection of `User` objects to enable data sharing among users working within a company.

## Properties
|           |                   |                                               |
| --------- | ----------------- | --------------------------------------------- |
| name      | `string`          |                                               |
| address   | `string`          |                                               |
| email     | `string`          |                                               |
| website   | `string`          |                                               |                        
| members   | `Array<string>`   | IDs of users belonging to the organisation    |
| admins    | `Array<string>`   | IDs of the admins                             |

## Organisation object
```
{
    name: "Wayne Enterprises",
    address: "Gotham",
    email: "bruce@wayne.com"
    website: "www.wayne.com"
    members: [
        "abcdefghijkl8901",
        "dgoiwnclkwjeflkjxic",
        "1xk329cdj3lkjsc31393lwc"
    ],
    admins: [
        "abcdefghijkl8901"
    ]
}
```

## Create an organisation
```
POST /orgs

REQUEST BODY
{
    name: "Wayne Enterprises",
    address: "Gotham",
    email: "bruce@wayne.com"
    website: "www.wayne.com" 
}

REQUEST HEADER
X-Access-Token sdlfjfskfjsldfjs31l3.sdflksjdfsl.3lkjsds
```

Upon success, the user account associated with the user token is set as Organisation admin, 
and is also added to `users` array.

## Get an organisation
```
GET /orgs/:orgID

REQUEST HEADER
X-Access-Token sdlfjfskfjsldfjs31l3.sdflksjdfsl.3lkjsds
```

Request succeeds only if the user account associated with the token is in the `users` lists of 
an organisation. Otherwise, a `403 Forbidden` response is sent with an appropriate `AdeptError`.

## Edit an organisation
```
PUT /orgs/:orgID

REQUEST BODY
{
    name: "Wayne Enterprises",
    address: "Gotham",
    email: "bruce@wayne.com"
    website: "www.wayne.com",
    members: [
        "abcdefghijkl8901",
        "dgoiwnclkwjeflkjxic",
        "1xk329cdj3lkjsc31393lwc"
    ],
    admins: [
        "abcdefghijkl8901"
    ]
}

REQUEST HEADER
X-Access-Token sdlfjfskfjsldfjs31l3.sdflksjdfsl.3lkjsds
```

Only admins can edit an organisation. Identity verification is done through access token.
This API call can be used to remove users from organisations, make someone an admin and other 
Organisation management tasks. All you need to do is add/remove user IDs from the relevant lists
and pass the whole object to the server.

## Delete an organisation
```
DELETE /orgs/:orgID

REQUEST HEADER
X-Access-Token sdlfjfskfjsldfjs31l3.sdflksjdfsl.3lkjsds
```

Only admins can delete an organisation. Identity verification is done through access token.