# Wall

In the cuboidal model of a [Building](building.md), the four lateral faces are abstracted as the external
walls. Each wall has its own insulation properties, windows, doors and material characteristics.

![Wall Models](assets/wall.png)

**A building must have four walls, and each wall in that group must have a unique orientation.** It's not possible
to have a building with two walls facing North.

## Properties

|                           |                   |                                                                               |
| ------------------------- | ----------------- | ----------------------------------------------------------------------------- |
| orientation               | `Direction`       | One of "north", "south", "east", "west"                                       |
| material                  | `WallMaterial`    | The construction material used in the wall. See *Material* below.             |
| cavity                    | `WallCavity`      | The nature of cavity in the wall. See *Cavity* below.                         |
| leaves                    | Array<`WallLeaf`> | The inner and outer leaves of a wall. See *Leaf* below.                       |
| windows                   | Array<`Window`>   | See [Window](window.md)                                                       |
| doors                     | Array<`Door`>     | See [Door](door.md)                                                           |


## Material
The material used in construction of the wall. Must be one of the following: masonry, timber, concrete or cladding.

```
"concrete"
```

## Cavity
Characteristics of the cavity inside the wall, if any. 

**Type** - Must be one of the following values: "none", "empty", "partially filled" or 
"fully filled".

**Width** - Width of the cavity in millimetres. 

**Insulation** - Insulation material used to fill in the cavity. See [Insulation](insulation.md).

```
{
    "type": "fully filled",
    "width": 200,
    "insulation": {
        "target": "cavity"
        "material": "rockwool",
        "thickness": 200                   
    }
}
```

## Leaf
Characteristics of the inner and outer leaf comprising the wall.

**Type** - "inner" or "outer"

**Thickness** - in millimetres

**Insulation** - Insulation used in the leaf. See [Insulation](insulation.md)

```
{
    "type": "inner",
    "thickness": 20
    "insulation": {
        "target": "leaf"
        "material": "polystyrene",
        "thickness": 20
    }
}
```

## The Wall Object
```
{
    "orientation": "west",
    "material": "concrete",
    "cavity": {
        "type": "fully filled",
        "width": 200,
        "insulation": {
            "target": "cavity"
            "material": "rockwool",
            "thickness": 200                   
        }
    },
    "leaves": [
        {
            "type": "inner",
            "thickness": 20
            "insulation": {
                "target": "leaf"
                "material": "polystyrene",
                "thickness": 20
            }
        },
        {
            "type": "outer",
            "thickness": 40
            "insulation": {
                "target": "leaf"
                "material": "polystyrene",
                "thickness": 40
            }
        }
    ],
    "windows": [
        {
            "frame": {
                "material": ???,
                "thickness": 20
            },
            "glazing": {
                "length": 15,
                "width": 30,
                "panes": [
                    {
                        "coating": ???,
                        "thickness": 12
                    },
                    {
                        "coating": ???,
                        "thickness": 4
                    }
                ],
                "cavities": [
                    {
                        "width": 20,
                        "fill": "argon",
                        "spacerMaterial": ???
                    },
                    {
                        "width": 20,
                        "fill": "argon",
                        "spacerMaterial": ???
                    }
                ]
            }
        },
        {
            "frame": {
                "material": ???,
                "thickness": 90
            },
            "glazing": {
                "length": 45,
                "width": 20,
                "panes": [
                    {
                        "coating": ???,
                        "thickness": 12
                    },
                    {
                        "coating": ???,
                        "thickness": 16
                    }
                ],
                "cavities": [
                    {
                        "width": 20,
                        "fill": "argon",
                        "spacerMaterial": ???
                    },
                    {
                        "width": 20,
                        "fill": "argon",
                        "spacerMaterial": ???
                    }
                ]
            }
        }
    ],
    "doors": [
        {
            "height": 120,
            "width": 200,
            "thickness": 20,
            "type": "accessible",
            "material": ???
        },
        {
            "height": 220,
            "width": 100,
            "thickness": 40,
            "type": "standard",
            "material": ???
        }
    ]
}
```