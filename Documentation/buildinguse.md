# Building Use

Tracking a building's use is essential for estimating energy use patterns and
carbon emissions.

## Properties
|                |              |                                                                                                |
| -------------- | ------------ | ---------------------------------------------------------------------------------------------- | 
| type           | `string`     | The intended use for the building. Options available through the `/options/building` endpoint. |
| occupancy      | `Occupancy`  | See below.                                                                                     |
| maxTemp        | `number`     | Maximum temperature the building should be run at. Optional. If no value is provided, set automatically based on `type`. |
| minTemp        | `number`     | Minimum temperature the building sbould be run at. Optional. If no value is provided, set automatically based on `type`. |
| ach            | `number`     | Air changes per hour. Default set to 8 (ignore for now)                                                         |
| vhr            | `number`     | Ventilation heat recovery efficiency  (ignore for now) |
| opensAt        | `number`     | Must be entered as 24-hr clock time. E.g. 530 for 5:30am |
| closesAt       | `number`     | Must be entered as 24-hr clock time. E.g. 2100 for 9:00pm |


## Occupancy
Users must provide the number of occupants expected (`count`) and the activity they will be engaged in (`OccupantActivity`). A list
of valid occupant activities is available through the `/options/building` UI Builder endpoint. E.g.

```
{
    count: 200,
    activity: "Light Machine Work"
}
```