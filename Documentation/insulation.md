# Insulation

## Properties

|                       |                   |                                                           |
| --------------------- | ----------------- | --------------------------------------------------------- |
| material              | `string`          | Can be: "rockwool", "polyurethane", "eps", "polystyrene"  |
| thickness             | `number`          | In millimetres.                                           |

## The Insulation Object
```
{
    material: "polystyrene",
    thickness: 100
}
```