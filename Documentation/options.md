# UI Builder

For a consistent building modeling system, it's imperative to ensure clients and
server offer the same range of possible inputs for all the parameters. So it's better
if parameter options are made available to the client by the server, rather than
hardcoding them in HTML. That's the purpose of UIBuilder endpoints.

## Building Options
Returns a comprehensive list of all the "dropdown-able" parameters required to 
create a building model. Since we are not dealing with sensitive user information
the request doesn't require an access-token.

```
GET https://www.ghs4.uk/options/building

RESPONSE (likely to change as we add/remove options)
{
  "buildingUse": [
    "Hospital",
    "Office",
    "School",
    "Warehouse"
  ],
  "direction": [
    "North",
    "East",
    "South",
    "West"
  ],
  "doorMaterial": [
    "Timber Softwood",
    "Timber Hardwood",
    "UPVC",
    "Steel"
  ],
  ...,
  ...,
  ...,
  "windowFrameMaterial": [
    "Timber Softwood",
    "Timber Hardwood",
    "UPVC",
    "Aluminium",
    "Steel"
  ]
}
```