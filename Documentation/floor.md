# Floor

![Floor Models](assets/floor.jpg)

## Properties

|                       |                   |                                   |
| --------------------- | ----------------- | --------------------------------- |
| type                  | `string`          | Can be: "solid" or "suspended"    |
| material              | `string`          | TBC                               |
| thickness             | `number`          | In millimetres.                   |
| insulation            | `Insulation`      | See [Insulation](insulation.md)   |
| voidDepth             | `number`          |                                   |

## The Floor Object
```
{
    type: "solid",
    material: null,
    thickness: 2000,
    insulation: {
        material: "polyurethane",
        thickness: 200
    },
    voidDepth: 0
}
```