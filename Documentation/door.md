# Door

## Properties

|                           |                   |                                                                               |
| ------------------------- | ----------------- | ----------------------------------------------------------------------------- |
| height                    | `number`          | In millimetres                                                                |
| width                     | `number`          | In millimetres                                                                |
| thickness                 | `number`          | In millimetres                                                                |
| type                      | `DoorType`        | Can be: "standard", "accessible" or "custom"                                  |                        
| material                  | `DoorMaterial`    | Can be: "softwood", "hardwood", "upvc", "steel"                               |

## The Door Object
```
{
    "height": 120,
    "width": 200,
    "thickness": 20,
    "type": "accessible",
    "material": "upvc"
}
```