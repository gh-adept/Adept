# Error Handling

All API errors are reported as an `AdeptError`. This is what an `AdeptError` looks
like:

```
{
    "adeptErrorType": "Duplicate",
    "message": {
        "firstName": "Animesh",
        "lastName": "Mishra",
        "email": "ash@mishra.io"
    }
}
```

`adeptErrorType` can be:

*   "Couldn't Delete",
*   "Couldn't Edit",
*   "Couldn't Find",
*   "Couldn't Get",
*   "Couldn't Init",
*   "Duplicate",
*   "Invalid Parameters",
*   "WTF"

`message` is usually either a one-line error message or in some cases, for instance 
a *Duplicate* error, the object that already exists in the database.

The `message` property should never ever have another `AdeptError` nested within it,
like so: 

```
{
    adeptErrorType: "Couldn't Find",
    message: {
        adeptErrorType: "Couldn't Get",
        message: "Requested resource doesn't exist."
    }
}
```
If you ever run into such errors, please report it. This is not normal behaviour and it's
a case of server-side overzealousness. The idea of the `message` property is to provide a
reason for why the operation failed, or if the reason is made obvious by `adeptErrorType`
then to provide some context (such as the object that already exists in the system for a `Duplicate` error).
Nesting errors within errors within errors misses the point and needs to be avoided at all times.

!!! note
    At times, the server might respond with just a plain one-line error message
    instead of a full `AdeptError` object. This happens when the error occurs
    in other services, such as our Climate API, which provide a part of Adept
    functionality. Such "third-party" errors are merely propagated through the
    response chain and sent to the client. 
    
    So the best way to handler errors would be look for a `adeptErrorType` property
    in the response error, if none is found that it's definitely not an 
    `AdeptError` and probably just plain text.


