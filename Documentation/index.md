# Adept

Adept allows users to assess how buildings can be made more sustainable, protected and resilient to impact of climate change
in the most cost-effective manner possible. This website explains how the data is structured in our mathematical models and 
enlists features that the server exposes through a REST API.

!!! Note
    This documentation describes an experiemental feature set that is likely to be changed and/or deprecated in the future.
    Always specify the version you are targeting using url parameters.

## Current release: 0.1
The stable release at the moment is version 0.1. Unless explicitly stated as fully qualified URL (e.g. https://www.ghs4.uk/signup),
all API endpoints listed on this site are relative to the base url `https://www.ghs4.uk/api/v1/`.

## Reference

* [Building](building.md) 
* [Walls](wall.md)
* [Roof](roof.md)
* [Floor](floor.md)
* [Windows](window.md)
* [Doors](door.md)
* Projections
    * Heat loss
    * Heating demand



