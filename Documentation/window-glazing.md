# Glazing

## Properties

|                           |                           |                                                |
| ------------------------- | ------------------------- | ---------------------------------------------- |
| height                    | `number`                  |                                                |
| width                     | `number`                  |                                                |
| panes                     | Array<`GlazingGlass`>     | See below                                      |
| cavities                  | Array<`GlazingCavity`>    | See below                                      |

## Glazing Glass

### Properties

**Coating** - reflective coating on the glass. Can be: ???

**Thickness** - in millimetres

```
{
    coating: "???",
    thickness: 20
}
```

## Glazing Cavity

### Properties

**Width** - width of the cavity in millimetres

**Fill** - The gas (or lack thereof) inside the cavity. Can be: "vacuum", "argon", "krypton", "air"

**SpacerMaterial** - material used to make the spacer bar. Can be: "aluminium", ???

```
{
    width: 12,
    fill: "vacuum",
    spacerMaterial: "aluminium"
}
```

## The Window Glazing Object
```
{
    "length": 15,
    "width": 30,
    "panes": [
        {
            "coating": ???,
            "thickness": 12
        },
        {
            "coating": ???,
            "thickness": 4
        }
    ],
    "cavities": [
        {
            "width": 20,
            "fill": "argon",
            "spacerMaterial": ???
        },
        {
            "width": 20,
            "fill": "argon",
            "spacerMaterial": ???
        }
    ]
}
```


