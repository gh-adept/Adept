# Control Systems

There are often multiple control systems - heating, cooling, water etc - working in tandem to keep a building running.

## Properties
|                |              |                                                                                                |
| -------------- | ------------ | ---------------------------------------------------------------------------------------------- | 
| type           | `string`     | Can be "Cooling", "Flooding" or "Heating". Values for drop-downs available via the Options API |
| fuel           | `string`     | Values available via Options API |
| efficiency     | `string`     | Percentage expressed as a fraction between 0 and 1. |


