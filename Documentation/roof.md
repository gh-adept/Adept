# Roof

## Properties

|                           |                           |                                                |
| ------------------------- | ------------------------- | ---------------------------------------------- |
| insulation                | `Insulation`              | Insulation used in the roof. See [Insulation](insulation.md) |

## The Roof Object
```
{
    insulation: {
        material: "rockwool",
        thickness: 100
    }
}
```


