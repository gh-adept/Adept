/**
 *  EKDataPoint.ts
 *  Adept.EnergyKit
 * 
 *  @created    December 31, 2016 by Animesh.
 *  @author     Animesh. hey@animesh.xyz
 *  @copyright  Copyright (c) 2016 Green Hill Products Limited
 *              All Rights Reserved  
 *  
 */

export class EKDataPoint
{
    public high: number
    public low: number
    public medium: number

    public constructor(lo: number, med: number, hi: number)
    {
        this.high = hi
        this.medium = med
        this.low = lo
    }
}

