/**
 *  EKDegreeDays.ts
 *  Adept.EnergyKit
 *      
 *  Using Hitchin's formula,
 *      D = Nm x (Tb - Tm) / 1 - e^(-k x (Tb - Tm))
 *  
 *  Nm  Number of days in the month
 *  Tb  Building base temperature
 *  Tm  Monthly mean temperature
 *  k   Location specific constant, taken to be mean value of 0.71
 *  
 *  @created    January 15, 2017
 *  @author     Animesh. hey@animesh.xyz
 *  @copyright  Copyright (c) 2017 Green Hill Products Limited
 *              All Rights Reserved
 * 
 */

export function For(period: string, baseTemp: number, meanTemp: number): number
{
    let numberOfDays = 0

    switch(period.toLowerCase()) {
    case "annual":
        numberOfDays = 365
        break
    case "january":
        numberOfDays = 31
        break
    case "february":
        numberOfDays = 28
        break
    case "march":
        numberOfDays = 31
        break
    case "april":
        numberOfDays = 30
        break
    case "may":
        numberOfDays = 31
        break
    case "june":
        numberOfDays = 30
        break
    case "july":
        numberOfDays = 31
        break
    case "august":
        numberOfDays = 31
        break
    case "september":
        numberOfDays = 30
        break
    case "october":
        numberOfDays = 31
        break
    case "november":
        numberOfDays = 30
        break
    case "december":
        numberOfDays = 31
        break
    }

    let tempDifferential = baseTemp - meanTemp
    return (numberOfDays * tempDifferential) / 1 - (Math.pow(Math.E, -(0.71 * tempDifferential)))
}