/**
 *  EKHeatLoss.ts
 *  Adept.EnergyKit
 *
 *  Heat loss from a building can be calculated as:
 *      H = Ht + Hv + Hi, where
 *      Ht -> heat loss from transmission through walls, windows, doors,
 *            ceilings and floors
 *      Hv -> heat loss due to ventilation 
 *      Hi -> heat loss due to infiltration
 *  
 *  This module defines methods to calculate a building's heat loss 
 *  signature, which can be used then to project future energy demands.
 * 
 *  @requires   BKBuilding, CKRequest, CKProjection, CKAveraging, CKEmissions,
 *              CKLocation, CKPeriod, CKVariable, CKService
 *  
 *  @created    December 1, 2016 by Animesh.
 *  @author     Animesh. hey@animesh.xyz
 *  @copyright  Copyright (c) Green Hill Products Limited
 *              All Rights Reserved.
 */

/** Density of air, in kilograms per cubic metre */
const densityAir = 1.225
/** Specific heat capacity of air, in Joules per kilogram Kelvin */
const CAir = 1.006

import { BKBuilding }      from "../BuildKit/BKBuilding"
import { CKRequest }       from "../ClimateKit/CKRequest"
import { CKProjection }    from "../ClimateKit/CKProjection"
import { CKAveraging }     from "../ClimateKit/CKAveraging"
import { CKEmissions }     from "../ClimateKit/CKEmissions"
import { CKLocation }      from "../ClimateKit/CKLocation"
import { CKPeriod }        from "../ClimateKit/CKPeriod"
import { CKVariable }      from "../ClimateKit/CKVariable"
import { CKService }       from "../ClimateKit/CKService"

export class EKHeatLoss
{
    public buildingID: string
    public projection: CKProjection
    public transmission: number
    public ventilation: number
    public infiltration: number

    public constructor(building: BKBuilding, projection: CKProjection)
    {
        this.projection = projection
        this.buildingID = building._id
        this.transmission = this.transmissionLoss(building, 28)
        this.ventilation = this.ventilationLoss(building, 28)
        this.infiltration = this.infiltrationLoss(building, 28)
    }

    /**
     *  Heat loss from transmission can be calculated as:
     *      Ht = area exposed x u-value x (insideTemp - outsideTemp)
     */
    private Ht(area: number, rValue: number, insideTemp: number, outsideTemp: number)
    {
        let uValue = 1 / rValue
        return area * uValue * (insideTemp - outsideTemp)
    }

    private transmissionLoss(building: BKBuilding, outsideTemp: number): number
    {
        let heatloss = 0
        let insideTemp = building.use.maxTemp
        let earthTemp = 15.0

        // Walls, windows and doors
        for (let wall of building.walls) {
            heatloss += this.Ht(wall.area(building), wall.rValue, insideTemp, outsideTemp)
            for (let window of wall.windows) {
                heatloss += this.Ht(window.area(), window.rValue, insideTemp, outsideTemp)
            }
            for (let door of wall.doors) {
                heatloss += this.Ht(door.area(), door.rValue, insideTemp, outsideTemp)
            }
        }

        // Roof, 15% extra because of radiation to space
        heatloss += 1.15 * (this.Ht(building.roof.area(building), building.roof.rValue, insideTemp, outsideTemp))

        // For floor, outside temperature should be replaced with earth temperature
        heatloss += this.Ht(building.floor.area(building), building.floor.rValue, insideTemp, earthTemp)

        return heatloss
    }

    /**
     *  The heat loss due to ventilation can be expressed as:
     *      Hv = (1 - β) * C * ρ * Qv * (ti - to), where
     *      
     *      β,  heat recovery efficiency (%) expressed as a fraction
     *      C,  specific heat of air (J/kg.K)
     *      ρ,  density of air (kg/cubic metre)
     *      Qv, fresh air flow through the room (cubic metre/second)
     *      ti, temperature indoors
     *      to, temperature outdoors
     *      
     *  A heat recovery efficiency of approx. 50% is common for a normal cross flow
     *  heat exchanger. For a rotating heat exchanger, the efficiency may exceed 80%.
     */
    private Hv(insideTemp: number, outsideTemp: number): number
    {
        // TODO: - recoveryEfficiency and airFlow should be taken in as parameters
        let recoveryEfficiency = 0.5
        let airVolumeFlow = 0.5
        return (1 - recoveryEfficiency) * CAir * densityAir * airVolumeFlow * (insideTemp - outsideTemp)
    }

    private ventilationLoss(building: BKBuilding, outsideTemp: number): number
    {
        return this.Hv(building.use.maxTemp, outsideTemp)
    }

    /**
     *  Due to leakage in the building construction, opening and closing of windows, etc.
     *  the air in the building shifts. As a rule of thumb the number of air shifts is set
     *  to 0.5 per hour. This value is hard to predict and depends on several variables:
     *  wind speeds, difference between inside and outside temperatures, quality of building
     *  construction etc. Heat loss caused by infiltration can be expressed as:
     *      Hi = C * ρ * n * V * (ti - to), where
     *      
     *      C,  specific heat capacity of air (J/kg.K)
     *      ρ,  density of air (kg/cubic metre)
     *      n,  number of air shifts, how many times the air is replaced in the room per second
     *          Set to 0.5 per hour as a rule of thumb.
     *      V,  volume of the room (cubic metre)
     *      ti, temperature indoors
     *      to, temperature outdoors
     */
    private Hi(volume: number, insideTemp: number, outsideTemp: number): number
    {
        let airShifts = 0.5
        return CAir * densityAir * airShifts * volume * (insideTemp - outsideTemp)
    }

    private infiltrationLoss(building: BKBuilding, outsideTemp: number): number
    {
        return this.Hi(building.volume(), building.use.maxTemp, outsideTemp)
    }
}

/**
 *  Air change rate can be defined as:
 *      n = (3600 * q)/V, where
 *      
 *      n,  air changes per hour
 *      q,  fresh air flow through the room (cubic metre per second)
 *      V,  volume of the room (cubic metre)
 */