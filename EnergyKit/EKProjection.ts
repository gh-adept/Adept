/**
 *  EKProjection.ts
 *  Adept.EnergyKit
 * 
 *  @requires   BKBuilding, EKDegreeDays
 *  
 *  @created    January 31, 2017
 *  @author     Animesh. hey@animesh.xyz
 *  @copyright  Copyright (c) 2016 Green Hill Products Limited
 *              All Rights Reserved.
 * 
 */

import { BKBuilding }       from "../BuildKit/BKBuilding"
import { EKDataPoint }      from "./EKDataPoint"
import { CKService }        from "../ClimateKit/CKService"
import { CKProjection }     from "../ClimateKit/CKProjection"
import { CKRequest }        from "../ClimateKit/CKRequest"
import { CKAveraging }      from "../ClimateKit/CKAveraging"
import { CKEmissions }      from "../ClimateKit/CKEmissions"
import { CKLocation }       from "../ClimateKit/CKLocation"
import { CKLocationType }   from "../ClimateKit/CKLocation"
import { CKPeriod }         from "../ClimateKit/CKPeriod"
import { CKVariable }       from "../ClimateKit/CKVariable"
import * as EKDegreeDays    from "./EKDegreeDays"

export class EKProjection
{
    public variable: string
    public building: BKBuilding
    public projections: Array<EKProjectionPoint>

    public constructor(building: BKBuilding)
    {
        this.variable = "Energy Use" 
        this.building = building
        this.projections = []
    }

    public compileData()
    {        
        this.request2020()
        this.request2040()
        this.request2060()
        this.request2080()
    }

    private request2020() {
        // For 2020
        let location = new CKLocation(this.building.locale.id, CKLocationType.GridSquare)
        let request2020 = new CKRequest(CKVariable.MeanDailyTemp, location, CKAveraging.Annual, null, CKPeriod.c2020)
        CKService.Query(request2020, (climateProjections: Array<CKProjection>) => {
            let consumptionHi, consumptionMed, consumptionLo: number
            let carbonHi, carbonMed, carbonLo: number
            let costHi, costMed, costLo: number

            if (climateProjections) {
                for (var proj of climateProjections) {
                    switch(proj.emissions) {
                    case 0: // High
                        consumptionHi = this.GetConsumption(proj.variants[2].value)
                        carbonHi = consumptionHi * 0.194
                        costHi = consumptionHi * 0.039
                        break
                    case 1: // Low
                        consumptionLo = this.GetConsumption(proj.variants[2].value)
                        carbonLo = consumptionLo * 0.194
                        costLo = consumptionLo * 0.039
                        break
                    case 2: // Medium
                        consumptionMed = this.GetConsumption(proj.variants[2].value)
                        carbonMed = consumptionMed * 0.194
                        costMed = consumptionMed * 0.039 
                        break
                    }
                }
                
                let projection = new EKProjectionPoint()
                projection.averaging = "Annual"
                projection.year = 2020
                projection.consumption = new EKDataPoint(consumptionLo, consumptionMed, consumptionHi)
                projection.carbonEmissions = new EKDataPoint(carbonLo, carbonMed, carbonHi)
                projection.cost = new EKDataPoint(costLo, costMed, costHi)
                this.projections.push(projection)
                this.request2030()
            }
        })
    }

    private request2030() {
        // For 2030
        let location = new CKLocation(this.building.locale.id, CKLocationType.GridSquare)
        let request2030 = new CKRequest(CKVariable.MeanDailyTemp, location, CKAveraging.Annual, null, CKPeriod.c2030)
        CKService.Query(request2030, (climateProjections: Array<CKProjection>) => {
            let consumptionHi, consumptionMed, consumptionLo: number
            let carbonHi, carbonMed, carbonLo: number
            let costHi, costMed, costLo: number

            if (climateProjections) {
                for (var proj of climateProjections) {
                    switch(proj.emissions) {
                    case 0: // High
                        consumptionHi = this.GetConsumption(proj.variants[2].value)
                        carbonHi = consumptionHi * 0.194
                        costHi = consumptionHi * 0.039
                        break
                    case 1: // Low
                        consumptionLo = this.GetConsumption(proj.variants[2].value)
                        carbonLo = consumptionLo * 0.194
                        costLo = consumptionLo * 0.039
                        break
                    case 2: // Medium
                        consumptionMed = this.GetConsumption(proj.variants[2].value)
                        carbonMed = consumptionMed * 0.194
                        costMed = consumptionMed * 0.039 
                        break
                    }
                }
                
                let projection = new EKProjectionPoint()
                projection.averaging = "Annual"
                projection.year = 2030
                projection.consumption = new EKDataPoint(consumptionLo, consumptionMed, consumptionHi)
                projection.carbonEmissions = new EKDataPoint(carbonLo, carbonMed, carbonHi)
                projection.cost = new EKDataPoint(costLo, costMed, costHi)
                this.projections.push(projection)
            }
        })
    }

    private request2040() {
        // For 2040
        let location = new CKLocation(this.building.locale.id, CKLocationType.GridSquare)
        let request2040 = new CKRequest(CKVariable.MeanDailyTemp, location, CKAveraging.Annual, null, CKPeriod.c2040)
        CKService.Query(request2040, (climateProjections: Array<CKProjection>) => {
            let consumptionHi, consumptionMed, consumptionLo: number
            let carbonHi, carbonMed, carbonLo: number
            let costHi, costMed, costLo: number

            if (climateProjections) {
                for (var proj of climateProjections) {
                    switch(proj.emissions) {
                    case 0: // High
                        consumptionHi = this.GetConsumption(proj.variants[2].value)
                        carbonHi = consumptionHi * 0.194
                        costHi = consumptionHi * 0.039
                        break
                    case 1: // Low
                        consumptionLo = this.GetConsumption(proj.variants[2].value)
                        carbonLo = consumptionLo * 0.194
                        costLo = consumptionLo * 0.039
                        break
                    case 2: // Medium
                        consumptionMed = this.GetConsumption(proj.variants[2].value)
                        carbonMed = consumptionMed * 0.194
                        costMed = consumptionMed * 0.039 
                        break
                    }
                }
                
                let projection = new EKProjectionPoint()
                projection.averaging = "Annual"
                projection.year = 2040
                projection.consumption = new EKDataPoint(consumptionLo, consumptionMed, consumptionHi)
                projection.carbonEmissions = new EKDataPoint(carbonLo, carbonMed, carbonHi)
                projection.cost = new EKDataPoint(costLo, costMed, costHi)
                this.projections.push(projection)
                this.request2050()
            }
        })
    }

    private request2050() {
        // For 2050
        let location = new CKLocation(this.building.locale.id, CKLocationType.GridSquare)
        let request2050 = new CKRequest(CKVariable.MeanDailyTemp, location, CKAveraging.Annual, null, CKPeriod.c2050)
        CKService.Query(request2050, (climateProjections: Array<CKProjection>) => {
            let consumptionHi, consumptionMed, consumptionLo: number
            let carbonHi, carbonMed, carbonLo: number
            let costHi, costMed, costLo: number

            if (climateProjections) {
                for (var proj of climateProjections) {
                    switch(proj.emissions) {
                    case 0: // High
                        consumptionHi = this.GetConsumption(proj.variants[2].value)
                        carbonHi = consumptionHi * 0.194
                        costHi = consumptionHi * 0.039
                        break
                    case 1: // Low
                        consumptionLo = this.GetConsumption(proj.variants[2].value)
                        carbonLo = consumptionLo * 0.194
                        costLo = consumptionLo * 0.039
                        break
                    case 2: // Medium
                        consumptionMed = this.GetConsumption(proj.variants[2].value)
                        carbonMed = consumptionMed * 0.194
                        costMed = consumptionMed * 0.039 
                        break
                    }
                }
                
                let projection = new EKProjectionPoint()
                projection.averaging = "Annual"
                projection.year = 2050
                projection.consumption = new EKDataPoint(consumptionLo, consumptionMed, consumptionHi)
                projection.carbonEmissions = new EKDataPoint(carbonLo, carbonMed, carbonHi)
                projection.cost = new EKDataPoint(costLo, costMed, costHi)
                this.projections.push(projection)
            }
        })
    }

    private request2060() {
        // For 2060
        let location = new CKLocation(this.building.locale.id, CKLocationType.GridSquare)
        let request2060 = new CKRequest(CKVariable.MeanDailyTemp, location, CKAveraging.Annual, null, CKPeriod.c2060)
        CKService.Query(request2060, (climateProjections: Array<CKProjection>) => {
            let consumptionHi, consumptionMed, consumptionLo: number
            let carbonHi, carbonMed, carbonLo: number
            let costHi, costMed, costLo: number

            if (climateProjections) {
                for (var proj of climateProjections) {
                    switch(proj.emissions) {
                    case 0: // High
                        consumptionHi = this.GetConsumption(proj.variants[2].value)
                        carbonHi = consumptionHi * 0.194
                        costHi = consumptionHi * 0.039
                        break
                    case 1: // Low
                        consumptionLo = this.GetConsumption(proj.variants[2].value)
                        carbonLo = consumptionLo * 0.194
                        costLo = consumptionLo * 0.039
                        break
                    case 2: // Medium
                        consumptionMed = this.GetConsumption(proj.variants[2].value)
                        carbonMed = consumptionMed * 0.194
                        costMed = consumptionMed * 0.039 
                        break
                    }
                }
                
                let projection = new EKProjectionPoint()
                projection.averaging = "Annual"
                projection.year = 2060
                projection.consumption = new EKDataPoint(consumptionLo, consumptionMed, consumptionHi)
                projection.carbonEmissions = new EKDataPoint(carbonLo, carbonMed, carbonHi)
                projection.cost = new EKDataPoint(costLo, costMed, costHi)
                this.projections.push(projection)
                this.request2070()
            }
        })
    }

    private request2070() {
        // For 2070
        let location = new CKLocation(this.building.locale.id, CKLocationType.GridSquare)
        let request2070 = new CKRequest(CKVariable.MeanDailyTemp, location, CKAveraging.Annual, null, CKPeriod.c2070)
        CKService.Query(request2070, (climateProjections: Array<CKProjection>) => {
            let consumptionHi, consumptionMed, consumptionLo: number
            let carbonHi, carbonMed, carbonLo: number
            let costHi, costMed, costLo: number

            if (climateProjections) {
                for (var proj of climateProjections) {
                    switch(proj.emissions) {
                    case 0: // High
                        consumptionHi = this.GetConsumption(proj.variants[2].value)
                        carbonHi = consumptionHi * 0.194
                        costHi = consumptionHi * 0.039
                        break
                    case 1: // Low
                        consumptionLo = this.GetConsumption(proj.variants[2].value)
                        carbonLo = consumptionLo * 0.194
                        costLo = consumptionLo * 0.039
                        break
                    case 2: // Medium
                        consumptionMed = this.GetConsumption(proj.variants[2].value)
                        carbonMed = consumptionMed * 0.194
                        costMed = consumptionMed * 0.039 
                        break
                    }
                }
                
                let projection = new EKProjectionPoint()
                projection.averaging = "Annual"
                projection.year = 2070
                projection.consumption = new EKDataPoint(consumptionLo, consumptionMed, consumptionHi)
                projection.carbonEmissions = new EKDataPoint(carbonLo, carbonMed, carbonHi)
                projection.cost = new EKDataPoint(costLo, costMed, costHi)
                this.projections.push(projection)
            }
        })
    }

    private request2080() {
        // For 2080
        let location = new CKLocation(this.building.locale.id, CKLocationType.GridSquare)
        let request2080 = new CKRequest(CKVariable.MeanDailyTemp, location, CKAveraging.Annual, null, CKPeriod.c2080)
        CKService.Query(request2080, (climateProjections: Array<CKProjection>) => {
            let consumptionHi, consumptionMed, consumptionLo: number
            let carbonHi, carbonMed, carbonLo: number
            let costHi, costMed, costLo: number

            if (climateProjections) {
                for (var proj of climateProjections) {
                    switch(proj.emissions) {
                    case 0: // High
                        consumptionHi = this.GetConsumption(proj.variants[2].value)
                        carbonHi = consumptionHi * 0.194
                        costHi = consumptionHi * 0.039
                        break
                    case 1: // Low
                        consumptionLo = this.GetConsumption(proj.variants[2].value)
                        carbonLo = consumptionLo * 0.194
                        costLo = consumptionLo * 0.039
                        break
                    case 2: // Medium
                        consumptionMed = this.GetConsumption(proj.variants[2].value)
                        carbonMed = consumptionMed * 0.194
                        costMed = consumptionMed * 0.039 
                        break
                    }
                }
                
                let projection = new EKProjectionPoint()
                projection.averaging = "Annual"
                projection.year = 2080
                projection.consumption = new EKDataPoint(consumptionLo, consumptionMed, consumptionHi)
                projection.carbonEmissions = new EKDataPoint(carbonLo, carbonMed, carbonHi)
                projection.cost = new EKDataPoint(costLo, costMed, costHi)
                this.projections.push(projection)
            }
        })
    }

    private GetConsumption(meanTemp: number): number
    {
        let degreeDays = EKDegreeDays.For("Annual", this.building.baseTemperatureHeating(), meanTemp)
        return this.building.EnergyConsumption(degreeDays)
    }
}

export class EKProjectionPoint
{
    public year: number
    public averaging: string
    public consumption: EKDataPoint
    public carbonEmissions: EKDataPoint
    public cost: EKDataPoint
}