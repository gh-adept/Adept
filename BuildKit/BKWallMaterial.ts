//
//  BKWallMaterial.ts
//  Adept
//
//  Created on November 20, 2016 by Animesh.
//  Copyright (c) 2016 Green Hill Products Limited
//  All Rights Reserved.
//

export enum BKWallMaterialType {
    BrickworkInner,
    BrickworkOuter,
    ConcreteAerated,
    ConcreteHigh,
    ConcreteLightweight,
    ConcreteMedium,
    ConcreteReinforced,
    MortarProtected,
    MortarExposed,
    GypsumHigh,
    GypsumLow,
    GypsumMedium,
    Sandstone,
    LimestoneHard,
    LimestoneSoft,
    TimberFraming,
    TimberHardwood,
    TimberSoftwood
}

export class BKWallMaterial
{
    public type: string
    public conductivity: number

    public constructor(type: BKWallMaterialType)
    {
        this.type = BKWallMaterial.ListTypes()[type]
        this.conductivity = BKWallMaterial.ConductivityOf(type)
    }

    public static ListTypes(): Array<string> {
        return [
            "Brickwork (inner)",
            "Brickwork (outer)",
            "Concrete - Aerated",
            "Concrete - High density",
            "Concrete - Lightweight",
            "Concrete - Medium density",
            "Concrete - Reinforced",
            "Gypsum - High density",
            "Gypsum - Low density",
            "Gypsum - Medium density",
            "Limestone - Hard",
            "Limestone - Soft",
            "Mortar - Protected",
            "Mortar - Exposed",
            "Sandstone",
            "Timber - Framing",
            "Timber - Hardwood",
            "Timber - Softwood"
        ]
    }

    public static From(type: string): BKWallMaterialType 
    {
        switch(type.toLowerCase()) {
        case "brickwork (inner)":
            return BKWallMaterialType.BrickworkInner
        case "brickwork (outer)":
            return BKWallMaterialType.BrickworkOuter
        case "concrete - aerated":
            return BKWallMaterialType.ConcreteAerated
        case "concrete - high density":
            return BKWallMaterialType.ConcreteHigh
        case "concrete - lightweight":
            return BKWallMaterialType.ConcreteLightweight
        case "concrete - medium density":
            return BKWallMaterialType.ConcreteMedium
        case "concrete - reinforced":
            return BKWallMaterialType.ConcreteReinforced
        case "mortar - protected":
            return BKWallMaterialType.MortarProtected
        case "mortar - exposed":
            return BKWallMaterialType.MortarExposed
        case "gypsum - high density":
            return BKWallMaterialType.GypsumHigh
        case "gypsum - low density":
            return BKWallMaterialType.GypsumLow
        case "gypsum - medium density":
            return BKWallMaterialType.GypsumMedium
        case "sandstone":
            return BKWallMaterialType.Sandstone
        case "limestone - hard":
            return BKWallMaterialType.LimestoneHard
        case "limestone - soft":
            return BKWallMaterialType.LimestoneSoft
        case "timber - framing":
            return BKWallMaterialType.TimberFraming
        case "timber - hardwood":
            return BKWallMaterialType.TimberHardwood
        case "timber - softwood":
            return BKWallMaterialType.TimberSoftwood
        }
    }

    public static ConductivityOf(type: BKWallMaterialType): number
    {
        switch(type) {
        case BKWallMaterialType.BrickworkInner:
            return 0.56
        case BKWallMaterialType.BrickworkOuter:
            return 0.77
        case BKWallMaterialType.ConcreteAerated:
            return 0.18
        case BKWallMaterialType.ConcreteHigh:
            return 1.93
        case BKWallMaterialType.ConcreteLightweight:
            return 0.57
        case BKWallMaterialType.ConcreteMedium:
            return 1.33
        case BKWallMaterialType.ConcreteReinforced:
            return 2.30
        case BKWallMaterialType.GypsumHigh:
            return 0.43
        case BKWallMaterialType.GypsumLow:
            return 0.18
        case BKWallMaterialType.GypsumMedium:
            return 0.30
        case BKWallMaterialType.LimestoneHard:
            return 1.70
        case BKWallMaterialType.LimestoneSoft:
            return 1.10
        case BKWallMaterialType.MortarExposed:
            return 0.94
        case BKWallMaterialType.MortarProtected:
            return 0.88
        case BKWallMaterialType.Sandstone:
            return 2.30
        case BKWallMaterialType.TimberFraming:
            return 0.12
        case BKWallMaterialType.TimberHardwood:
            return 0.18
        case BKWallMaterialType.TimberSoftwood:
            return 0.13
        }
    }
}