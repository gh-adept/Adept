//
//  BKGlazingCavityFill.ts
//  Adept
//
//  Created on November 20, 2016 by Animesh.
//  Copyright (c) 2016 Green Hill Products Limited
//  All Rights Reserved.
//

export enum BKGlazingCavityFillType {
    Vacuum,
    Air,
    Argon,
    Krypton
}

export class BKGlazingCavityFill
{
    public type: string
    public conductivity: number

    public constructor(type: BKGlazingCavityFillType)
    {
        this.type = BKGlazingCavityFill.ListFillTypes()[type]
        this.conductivity = BKGlazingCavityFill.ConductivityOf(type)
    }

    public static ListFillTypes(): Array<string> {
        return [
            "Vacuum",
            "Air",
            "Argon",
            "Krypton"
        ]
    }

    public static FromFill(fill: string): BKGlazingCavityFillType
    {
        switch(fill.toLowerCase()) {
        case "vacuum":
            return BKGlazingCavityFillType.Vacuum
        case "air":
            return BKGlazingCavityFillType.Air
        case "argon":
            return BKGlazingCavityFillType.Argon
        case "krypton":
            return BKGlazingCavityFillType.Krypton
        }
    }

    public static ConductivityOf(type: BKGlazingCavityFillType): number
    {
        switch(type) {
        case BKGlazingCavityFillType.Vacuum:
            return 0
        case BKGlazingCavityFillType.Air:
            return 0.024
        case BKGlazingCavityFillType.Argon:
            return 0.016
        case BKGlazingCavityFillType.Krypton:
            return 0.0088
        }
    }
}