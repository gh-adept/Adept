//
//  BKInsulationMaterial.ts
//  Adept
//
//  Created on November 20, 2016 by Animesh.
//  Copyright (c) 2016 Green Hill Products Limited
//  All rights reserved.
//

export enum BKInsulationMaterialType {
    Aerogel,
    CellularGlass,
    Cellulose,
    FoamPhenolic,
    FoamPolyurethane,
    H2FoamLite,
    Hemp,
    Hempcrete,
    MineralWoolGlass,
    MineralWoolRock,
    PolystyreneExpanded,
    PolystyreneExtruded,
    Straw,
    WoodFibreRigid,
    WoodFibreFlexible,
    Wool
}

export class BKInsulationMaterial 
{
    public type: string
    public conductivity: number

    public constructor(type: BKInsulationMaterialType)
    {
        this.type = BKInsulationMaterial.ListTypes()[type]
        this.conductivity = BKInsulationMaterial.ConductivityOf(type)
    }

    public static ListTypes(): Array<string>
    {
        return [
            "Aerogel",
            "Cellular Glass",
            "Cellulose",
            "Expanded Polystyrene (EPS)",
            "Extruded Polystyrene (XPS)",
            "Flexible Wood Fibre",
            "Glass Mineral Wool",
            "H2FoamLite",
            "Hemp",
            "Hempcrete",
            "Phenolic Foam",
            "Polyurethane Foam",
            "Rigid Wood Fibre",
            "Rock Mineral Wool",
            "Straw",
            "Wool"
        ]
    }

    public static From(type: string): BKInsulationMaterialType
    {
        switch(type.toLowerCase()) {
        case "aerogel":
            return BKInsulationMaterialType.Aerogel
        case "cellular glass":
            return BKInsulationMaterialType.CellularGlass
        case "cellulose":
            return BKInsulationMaterialType.Cellulose
        case "phenolic foam":
            return BKInsulationMaterialType.FoamPhenolic
        case "polyurethane foam":
            return BKInsulationMaterialType.FoamPolyurethane
        case "h2foamlite":
            return BKInsulationMaterialType.H2FoamLite
        case "hemp":
            return BKInsulationMaterialType.Hemp
        case "hempcrete":
            return BKInsulationMaterialType.Hempcrete
        case "glass mineral wool":
            return BKInsulationMaterialType.MineralWoolGlass
        case "rock mineral wool":
            return BKInsulationMaterialType.MineralWoolRock
        case "expanded polystyrene (eps)":
            return BKInsulationMaterialType.PolystyreneExpanded
        case "extruded polystyrene (xps)":
            return BKInsulationMaterialType.PolystyreneExtruded
        case "straw":
            return BKInsulationMaterialType.Straw
        case "rigid wood fibre":
            return BKInsulationMaterialType.WoodFibreRigid
        case "flexible wood fibre":
            return BKInsulationMaterialType.WoodFibreFlexible
        case "wool":
            return BKInsulationMaterialType.Wool
        }
    }

    public static ConductivityOf(type: BKInsulationMaterialType)
    {
        switch(type) {
        case BKInsulationMaterialType.Aerogel:
            return 0.014
        case BKInsulationMaterialType.CellularGlass:
            return 0.041
        case BKInsulationMaterialType.Cellulose:
            return 0.038
        case BKInsulationMaterialType.FoamPhenolic:
            return 0.020
        case BKInsulationMaterialType.FoamPolyurethane:
            return 0.025
        case BKInsulationMaterialType.H2FoamLite:
            return 0.039
        case BKInsulationMaterialType.Hemp:
            return 0.040
        case BKInsulationMaterialType.Hempcrete:
            return 0.060
        case BKInsulationMaterialType.MineralWoolGlass:
            return 0.035
        case BKInsulationMaterialType.MineralWoolRock:
            return 0.038
        case BKInsulationMaterialType.PolystyreneExpanded:
            return 0.036
        case BKInsulationMaterialType.PolystyreneExtruded:
            return 0.035
        case BKInsulationMaterialType.Straw:
            return 0.080
        case BKInsulationMaterialType.WoodFibreFlexible:
            return 0.038
        case BKInsulationMaterialType.WoodFibreRigid:
            return 0.038
        case BKInsulationMaterialType.Wool:
            return 0.038
        }
    }
}