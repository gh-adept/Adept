/**
 *  BuildingAPI.ts
 *  Adept
 *  
 *  @requires   express, jsonwebtoken
 *  @requires   AdeptError, AdeptErrorType, Building, Locale, Router
 * 
 *  @created    November 20, 2016
 *  @author     Animesh. ash@mishra.io
 *  @copyright  Copyright (c) 2016 Green Hill Products Limited.
 *              All Rights Reserved.
 */

import * as Express      from "express"
import * as JWT          from "jsonwebtoken"

import { AdeptError }    from "../Helpers/AdeptError"
import { AdeptErrorType} from "../Helpers/AdeptError"
import { BKBuilding }    from "../BuildKit/BKBuilding"
import { BKLocale }      from "../BuildKit/BKLocale"
import { JSONPurpose }   from "../Helpers/JSONPurpose"
import { Router }        from "../Controllers/Router"

/**
 *  @route  /buildings
 *  @http   POST 
 */
export function Create(request: Express.Request, response: Express.Response, next: Express.NextFunction)
{
    // Get ID of the user making the request and set it as 
    // new building's `parent`
    let token        = request.headers["x-access-token"]
    let userID       = JWT.decode(token, { complete: true }).payload._id
    let parent       = userID

    // Initialise the building
    let building     = new BKBuilding(JSONPurpose.InitFromClient, request.body, parent)

    Router.dataService.add("buildings", building, null, (data, error) => {
        if (error) {
            error.message = Router.Sanitize(error.message)
            response.status(404).json(error)
        }
        else {
            if (data.ok) {
                building._id = data.id
                building._rev = data.rev
                response.status(200).json(building)
            }
            else {
                response.status(404).json(new AdeptError(AdeptErrorType.CouldntInit, Router.Sanitize(data)))
            } 
        }
    })
}

/**
 *  @route  /buildings
 *  @http   GET
 */
export function GetAll(request: Express.Request, response: Express.Response, next: Express.NextFunction)
{
    let token   = request.headers["x-access-token"]
    let userID  = JWT.decode(token, { complete: true }).payload._id

    let parentQuery = {
        "parent": userID
    }

    let sharedWithQuery = {
        "sharedWith": {
            "$elemMatch": {
                "$eq": userID
            }
        }
    }

    let predicate = {
        "selector": {
            "$or": [ parentQuery, sharedWithQuery ]
        }
    }

    Router.dataService.search("buildings", predicate, (queryResult, error) => {
        if (error) {
            response.status(404).json(new AdeptError(AdeptErrorType.CouldntGet, error))
        }
        else {
            let buildings = []
            for (var building of queryResult.docs) {
                buildings.push(new BKBuilding(JSONPurpose.InitFromDb, building, building.parent))
            }
            response.status(200).json(buildings)
        }
    })
}

/**
 *  @route  /buildings/:buildingID
 *  @http   GET
 */
export function Get(request: Express.Request, response: Express.Response, next: Express.NextFunction)
{
    let token   = request.headers["x-access-token"]
    let userID  = JWT.decode(token, { complete: true }).payload._id

    let buildingID = request.params.buildingID

    Router.dataService.get("buildings", buildingID, (data, error) => {
        if (error) {
            error.message = Router.Sanitize(error.message)
            response.status(404).json(error)
        }
        else {
            if (data.parent != userID) {
                let message = "For now, users can only access building models they have created themselves."
                let error = new AdeptError(AdeptErrorType.CouldntGet, message)
                response.status(403).json(error)
            }
            else {
                let building = new BKBuilding(JSONPurpose.InitFromDb, data, data.parent)
                response.status(200).json(building)
            }
        }
    })
}

/**
 *  @route  /buildings/:buildingID
 *  @http   PUT
 */
export function Update(request: Express.Request, response: Express.Response, next: Express.NextFunction)
{
    let token   = request.headers["x-access-token"]
    let userID  = JWT.decode(token, { complete: true }).payload._id

    let id = request.params.buildingID

    // Get the object to be updated
    Router.dataService.get("buildings", id, (data, error) => {
        if(error) {
            error.message = Router.Sanitize(error.message)
            delete error.message._id
            response.status(404).json(error)
        }
        else {
            if (data.parent != userID) {
                let message = "For now, users can only edit building models they have created themselves."
                let error = new AdeptError(AdeptErrorType.CouldntEdit, message)
                response.status(403).json(error)
            }
            else {
                let buildingInDB = new BKBuilding(JSONPurpose.InitFromDb, data, data.parent)
                let newBuilding = new BKBuilding(JSONPurpose.Update, request.body, buildingInDB.parent)
                Router.dataService.add("buildings", newBuilding, null, (data, error) => {
                    if(error) {
                        error.message = Router.Sanitize(error.message)
                        delete error.message._id
                        response.status(404).json(error)
                    }
                    else {
                        if (data.ok) {
                            newBuilding._rev = data.rev
                            response.status(200).json(newBuilding)
                        }
                        else {
                            response.status(403).json(data)
                        }
                    }
                })
            }
        }
    })
}

/**
 *  @route  /buildings/:buildingID
 *  @http   DELETE
 */
export function Delete(request: Express.Request, response: Express.Response, next: Express.NextFunction)
{
    let token   = request.headers["x-access-token"]
    let userID  = JWT.decode(token, { complete: true }).payload._id

    let id = request.params.buildingID

    Router.dataService.get("buildings", id, (building, error) => {
        if (error) {
            error.message = Router.Sanitize(error.message)
            delete error.message._id
            response.status(404).json(error)
        }
        else {
            if (building.parent != userID) {
                let message = "For now, users can only delete building models they have created themselves."
                let error = new AdeptError(AdeptErrorType.CouldntDelete, message)
                response.status(403).json(error)
            }
            else {
                Router.dataService.delete("buildings", id, (error) => {
                    if(error) {
                        error.message = Router.Sanitize(error.message)
                        delete error.message._id
                        response.status(404).json(error)
                    }
                    else {
                        response.status(200).json("Successfully deleted building with ID: " + id)
                    }
                })
            }
        }
    })
}