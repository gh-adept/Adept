//
//  BKWindow.ts
//  Adept
//
//  Created on November 20, 2016 by Animesh.
//  Copyright (c) 2016 Green Hill Products Limited
//  All Rights Reserved.
//

import { BKGlazing }     from "./BKGlazing"
import { BKWindowFrame } from "./BKWindowFrame"

export class BKWindow
{
    public glazing: BKGlazing
    public frame: BKWindowFrame

    /** Thermal resistance is thickness (in metres) divided by conductivity.
     *  Unit: m2K/W
     */
    public rValue: number
    
    public constructor(json: any)
    {
        this.glazing = new BKGlazing(json.glazing)
        this.frame   = new BKWindowFrame(json.frame)
        this.rValue  = this.thermalResistance()
    }

    public thermalResistance(): number
    {
        let rValueGlazing = this.glazing.thermalResistance()
        let rValueFrame = this.frame.thermalResistance(BKWindowFrame.From(this.frame.material))

        return rValueGlazing + rValueFrame
    }

    public area(): number
    {
        return this.glazing.area() + this.frame.area(this.glazing.height, this.glazing.width)   
    }
}