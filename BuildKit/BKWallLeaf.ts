//
//  BKWallLeaf.ts
//  Adept
//
//  Created on November 20, 2016 by Animesh.
//  Copyright (c) 2016 Green Hill Products Limited.
//  All Rights Reserved.
//

import { BKInsulation }   from "./BKInsulation"
import { BKWallMaterial } from "./BKWallMaterial"

export enum BKWallLeafType {
    Inner,
    Outer
}

export class BKWallLeaf
{
    public type         : string
    public thickness    : number
    public material     : string
    public insulation   : BKInsulation
    public rValue       : number

    public constructor(json: any)
    {
        this.type       = json.type
        this.thickness  = json.thickness
        this.material   = json.material
        
        let leafMaterial = new BKWallMaterial(BKWallMaterial.From(json.material))
        let insulation  = new BKInsulation(json.insulation)
        this.insulation = insulation
        this.rValue     = this.thermalResistance(leafMaterial, insulation)
    }

    public thermalResistance(material: BKWallMaterial, insulation: BKInsulation)
    {
        if (insulation) {
            return ((this.thickness / 1000) / material.conductivity) + insulation.rValue
        }
        else {
            return (this.thickness / 1000) / material.conductivity
        }
    }

    public static ListTypes(): Array<string> {
        return [
            "Inner",
            "Outer"
        ]
    }

    public static From(type: string): BKWallLeafType
    {
        switch(type.toLowerCase()) {
        case "inner":
            return BKWallLeafType.Inner
        case "outer":
            return BKWallLeafType.Outer
        }
    }
}