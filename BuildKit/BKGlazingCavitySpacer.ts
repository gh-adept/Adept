//
//  BKGlazingCavitySpacer.ts
//  Adept
//
//  Created on November 20, 2016 by Animesh.
//  Copyright (c) 2016 Green Hill Products Limited
//  All Rights Reserved.
//

export enum BKGlazingCavitySpacerMaterial {
    Aluminium
}

export class BKGlazingCavitySpacer
{
    public material: string
    public conductivity: number
    
    public constructor(material: string)
    {
        this.material = material
        let spacer = BKGlazingCavitySpacer.FromSpacer(material)
        this.conductivity = BKGlazingCavitySpacer.ConductivityOf(spacer)
    }

    public static ListMaterials(): Array<string> {
        return [
            "Aluminium"
        ]
    }

    public static FromSpacer(spacer: string): BKGlazingCavitySpacerMaterial
    {
        switch(spacer.toLowerCase()) {
        case "aluminium":
            return BKGlazingCavitySpacerMaterial.Aluminium
        }
    }

    public static ConductivityOf(type: BKGlazingCavitySpacerMaterial): number
    {
        switch(type) {
        case BKGlazingCavitySpacerMaterial.Aluminium:
            return 205
        }
    }
}