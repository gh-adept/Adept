//
//  BKInsulation.ts
//  Adept
//
//  Created on November 20, 2016 by Animesh.
//  Copyright (c) 2016 Green Hill Products Limited
//  All Rights Reserved.
//

import { BKInsulationMaterial } from "./BKInsulationMaterial"

export class BKInsulation
{
    public material: string
    public thickness: number
    public rValue: number

    public constructor(json: any)
    {
        this.thickness  = json.thickness
        this.material   = json.material

        // Calculate rValue
        let insMaterial = new BKInsulationMaterial(BKInsulationMaterial.From(json.material))
        this.rValue   = this.thermalResistance(insMaterial)
    }

    public thermalResistance(material: BKInsulationMaterial): number
    {
        return (this.thickness / 1000) / material.conductivity
    }
}