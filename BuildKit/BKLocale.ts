//
//  BKLocale.ts
//  Adept
//  
//  Created on November 22, 2016 by Animesh.
//  Copyright (c) 2016 Green Hill Products Limited
//  All Rights Reserved.
//

enum BKLocaleType {
    Region,
    Grid
}

export class BKLocale 
{
    public type     : string
    public id       : string
    public latitude : number
    public longitude: number

    public constructor(json: any)
    {
        this.type       = json.type
        this.id         = json.id 
        this.latitude   = json.latitude
        this.longitude  = json.longitude
    }

    public static From(type: string): BKLocaleType
    {
        switch(type.toLowerCase()) {
        case "region":
            return BKLocaleType.Region
        case "grid":
            return BKLocaleType.Grid
        }
    }

    public static ListTypes(): Array<string> {
        return [
            "Region",
            "Grid"
        ]
    }
} 