//
//  BKDoorMaterial.ts
//  Adept
//
//  Created on November 20, 2016 by Animesh.
//  Copyright (c) 2016 Green Hill Products Limited
//  All Rights Reserved.
//

export enum BKDoorMaterialType {
    Aluminium,
    Hardwood,
    PVC,
    Softwood,
    Steel
}

export class BKDoorMaterial
{
    public type: string

    /** 
     *  Rate at which heat passes through a material.
     *  Measure in Watts per square metre of surface area for a 
     *  temperature gradient of 1 Kelvin for every metre thickness.
     *  Unit: W/mK
     */
    public conductivity: number

    public constructor(type: BKDoorMaterialType)
    {
        this.type = BKDoorMaterial.ListTypes()[type]
        this.conductivity = BKDoorMaterial.ConductivityOf(type)
    }

    public static ListTypes(): Array<string>
    {
        return [
            "Aluminium",
            "Hardwood",
            "PVC",
            "Softwood",
            "Steel"
        ]
    }

    public static From(type: string): BKDoorMaterialType
    {
        switch(type.toLowerCase()) {
        case "aluminium":
            return BKDoorMaterialType.Aluminium
        case "hardwood":
            return BKDoorMaterialType.Hardwood
        case "pvc":
            return BKDoorMaterialType.PVC
        case "softwood":
            return BKDoorMaterialType.Softwood
        case "steel":
            return BKDoorMaterialType.Steel
        }
    }

    public static ConductivityOf(type: BKDoorMaterialType): number
    {
        switch(type) {
        case BKDoorMaterialType.Aluminium:
            return 45.00
        case BKDoorMaterialType.Hardwood:
            return 0.16
        case BKDoorMaterialType.PVC:
            return 0.16
        case BKDoorMaterialType.Softwood:
            return 0.14
        case BKDoorMaterialType.Steel:
            return 45.00
        }
    }
}