//
//  BKDoorType.ts
//  Adept
//
//  Created on November 20, 2016 by Animesh.
//  Copyright (c) 2016 Green Hill Products Limited
//  All Rights Reserved.
//

export enum BKDoorTypeIs {
    Standard,
    Accessible,
    Custom
}

export class BKDoorType
{
    public description: string

    public constructor(type: BKDoorTypeIs)
    {
        this.description = BKDoorType.ListTypes()[type]
    }

    public static ListTypes(): Array<string>
    {
        return [
            "Standard",
            "Accessible",
            "Custom"
        ]
    }

    public static From(type: string): BKDoorTypeIs
    {
        switch(type.toLowerCase()) {
        case "standard":
            return BKDoorTypeIs.Standard
        case "accessible":
            return BKDoorTypeIs.Accessible
        case "custom":
            return BKDoorTypeIs.Custom
        }
    }
}