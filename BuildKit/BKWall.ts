//
//  BKWall.ts
//  Adept
//
//  Created on November 20, 2016 by Animesh.
//  Copyright (c) 2016 Green Hill Products Limited
//  All Rights Reserved.
//

import { AdeptError }     from "../Helpers/AdeptError"
import { BKBuilding }     from "./BKBuilding"
import { BKWallCavity }   from "./BKWallCavity"
import { BKWallMaterial } from "./BKWallMaterial"
import { BKWallLeaf }     from "./BKWallLeaf"
import { BKDirection }    from "./BKDirection"
import { BKWindow }       from "./BKWindow"
import { BKDoor }         from "./BKDoor"

export class BKWall
{
    public orientation : string
    public cavity      : BKWallCavity
    public leaves      : Array<BKWallLeaf>
    public windows     : Array<BKWindow>
    public doors       : Array<BKDoor>
    public rValue      : number

    public constructor(json: any) {
        this.orientation    = json.orientation
        this.cavity         = new BKWallCavity(json.cavity)
        this.leaves         = this.initLeaves(json.leaves)
        this.windows        = this.initWindows(json.windows)
        this.doors          = this.initDoors(json.doors)
        this.rValue         = this.thermalResistance()
    }

    public initDoors(json: any): Array<BKDoor>
    {
        let doors = []
        for (var door of json) {
            doors.push(new BKDoor(door))
        }
        return doors
    }

    public initLeaves(json: any): Array<BKWallLeaf>
    {
        let leaves = []
        for (var leaf of json) {
            leaves.push(new BKWallLeaf(leaf))
        }
        return leaves
    }

    public initWindows(json: any): Array<BKWindow>
    {
        let windows = []
        for (var window of json) {
            windows.push(new BKWindow(window))
        }
        return windows
    }

    public thermalResistance(): number
    {
        let rValue = 0
        for (let leaf of this.leaves) {
            rValue += leaf.rValue
        }

        rValue += this.cavity.rValue

        return rValue
    }

    public area(parent: BKBuilding): number
    {
        let totalArea = 0
        switch(this.orientation.toLowerCase()) {
        case "north":
        case "south":
            totalArea = parent.length * (parent.storeyHeight * parent.storeys)
            return this.leafArea(totalArea)

        case "east":
        case "west":
            totalArea = parent.width * (parent.storeyHeight * parent.storeys)
            return this.leafArea(totalArea)

        }
    }

    private leafArea(totalArea: number): number
    {
        for (let window of this.windows) {
            totalArea -= window.area()
        }
        for (let door of this.doors) {
            totalArea -= door.area()
        }

        return totalArea
    }
}
