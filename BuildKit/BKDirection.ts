//
//  BKDirection.ts
//  Adept
//
//  Created on November 20, 2016 by Animesh.
//  Copyright (c) 2016 Green Hill Products Limited
//  All Rights Reserved.
//

export enum BKDirectionType {
    North,
    East,
    South,
    West
}

export class BKDirection
{
    public type: string

    public constructor(type: BKDirectionType)
    {
        this.type = BKDirection.ListTypes()[type]
    }
    
    public static ListTypes(): Array<string> {
        return [
            "North",
            "East",
            "South",
            "West"
        ]
    }

    public static From(type: string): BKDirectionType
    {
        switch(type.toLowerCase()) {
        case "north":
            return BKDirectionType.North
        case "east":
            return BKDirectionType.East
        case "south":
            return BKDirectionType.South
        case "west":
            return BKDirectionType.West
        }
    }
}