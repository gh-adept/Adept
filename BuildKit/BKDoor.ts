//
//  BKDoor.ts
//  Adept
//  
//  Created on November 20, 2016 by Animesh.
//  Copyright (c) 2016 Green Hill Products Limited
//  All Rights Reserved.
//

import { BKDoorType } from "./BKDoorType"
import { BKDoorMaterial } from "./BKDoorMaterial"

export class BKDoor
{
    public height       : number
    public width        : number
    public thickness    : number
    public type         : string
    public material     : string
    public rValue       : number

    public constructor(json: any)
    {
        this.type       = json.type
        this.height     = json.height
        this.width      = json.width
        this.thickness  = json.thickness
        this.material   = json.material
        let doorMaterial = new BKDoorMaterial(BKDoorMaterial.From(json.material))
        this.rValue     = this.thermalResistance(doorMaterial)
    }

    public thermalResistance(material: BKDoorMaterial): number
    {
        return (this.thickness / 1000) / material.conductivity
    }

    public area(): number 
    {
        return this.height * this.width
    }
}