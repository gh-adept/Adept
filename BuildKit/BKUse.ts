//
//  BKUse.ts
//  Adept
//
//  Created on November 23, 2016 by Animesh.
//  Copyright (c) 2016 Green Hill Products Limited
//  All Rights Reserved.
//

import { BKOccupancy } from "./BKOccupancy"

export enum BKUseType
{
    Airport,
    Bank,
    Dwelling,
    Factory,
    FireStation,
    Garage,
    Hospital,
    Hotel,
    Laundry,
    Library,
    Museum,
    Office,
    RailwayStation,
    School,
    ShoppingMall,
    SportsHall,
    SwimmingPool
}

export class BKUse 
{
    public type         : string
    public occupancy    : BKOccupancy
    public desiredTemp  : number
    public maxTemp      : number
    public minTemp      : number

    /** Air change per hour */
    public ach: number = 8
    /** Ventilation heat recovery efficiency */
    public vhr: number

    /** Must be entered as 24-hr clock time. E.g. 1530 for 3:30pm */
    public opensAt : number
    /** Must be entered as 24-hr clock time. E.g. 2100 for 9pm */
    public closesAt: number

    public hoursPerDay     : number
    public workdaysPerWeek : number = 5
    public workweeksPerYear: number = 50

    public constructor(json: any)
    {
        this.type           = json.type
        let useType         = BKUse.From(json.type)
        this.desiredTemp    = json.desiredTemp
        
        if (json.minTemp) {
            this.minTemp    = json.minTemp
        }
        else {
            this.minTemp    = BKUse.defaultMinTemperature(useType)
        }

        if (json.maxTemp) {
            this.maxTemp    = json.maxTemp
        }
        else {
            this.maxTemp    = BKUse.defaultMaxTemperature(useType)
        }

        this.occupancy      = new BKOccupancy(json.occupancy)
        this.opensAt        = json.opensAt
        this.closesAt       = json.closesAt
        let openingDecimal  = Math.floor(json.opensAt / 100) + ((json.opensAt % 100) / 60)
        let closingDecimal  = Math.floor(json.closesAt / 100) + ((json.closesAt % 100) / 60)
        this.hoursPerDay    = closingDecimal - openingDecimal
    }

    public static ListTypes(): Array<string> {
        return [
            "Airport",
            "Bank",
            "Dwelling",
            "Factory",
            "Fire Station",
            "Garage",
            "Hospital",
            "Hotel",
            "Laundry",
            "Library",
            "Museum",
            "Office",
            "Railway Station",
            "School",
            "Shopping Mall",
            "Sports Hall",
            "Swimming Pool"
        ]
    }

    public static From(type: string): BKUseType 
    {
        switch (type.toLowerCase()) {
        case "airport":
            return BKUseType.Airport
        case "bank":
            return BKUseType.Bank
        case "dwelling":
            return BKUseType.Dwelling
        case "factory":
            return BKUseType.Factory
        case "fire station":
            return BKUseType.FireStation
        case "garage":
            return BKUseType.Garage
        case "hospital":
            return BKUseType.Hospital
        case "hotel":
            return BKUseType.Hotel
        case "laundry":
            return BKUseType.Laundry
        case "library":
            return BKUseType.Library
        case "museum":
            return BKUseType.Museum
        case "office":
            return BKUseType.Office
        case "railway station":
            return BKUseType.RailwayStation
        case "school":
            return BKUseType.School
        case "shopping mall":
            return BKUseType.ShoppingMall
        case "sports hall":
            return BKUseType.SportsHall
        case "swimming pool":
            return BKUseType.SwimmingPool
        default:
            return BKUseType.Office
        }
    }

    private static defaultMinTemperature(type: BKUseType): number
    {
        switch (type) {
        case BKUseType.Airport:
            return 12.0
        case BKUseType.Bank:
            return 19.0
        case BKUseType.Dwelling:
            return 17.0
        case BKUseType.Factory:
            return 13.0
        case BKUseType.FireStation:
            return 20.0
        case BKUseType.Garage:
            return 16.0
        case BKUseType.Hospital:
            return 17.0
        case BKUseType.Hotel:
            return 19.0
        case BKUseType.Laundry:
            return 16.0
        case BKUseType.Library:
            return 15.0
        case BKUseType.Museum:
            return 19.0
        case BKUseType.Office:
            return 21.0
        case BKUseType.RailwayStation:
            return 12.0
        case BKUseType.School:
            return 19.0
        case BKUseType.ShoppingMall:
            return 12.0
        case BKUseType.SportsHall:
            return 13.0
        case BKUseType.SwimmingPool:
            return 23.0
        }
    }

    private static defaultMaxTemperature(type: BKUseType): number
    {
        switch (type) {
        case BKUseType.Airport:
            return 19.0
        case BKUseType.Bank:
            return 22.0
        case BKUseType.Dwelling:
            return 22.0
        case BKUseType.Factory:
            return 21.0
        case BKUseType.FireStation:
            return 23.0
        case BKUseType.Garage:
            return 19.0
        case BKUseType.Hospital:
            return 24.0
        case BKUseType.Hotel:
            return 22.0
        case BKUseType.Laundry:
            return 21.0
        case BKUseType.Library:
            return 23.0
        case BKUseType.Museum:
            return 21.0
        case BKUseType.Office:
            return 23.0
        case BKUseType.RailwayStation:
            return 23.0
        case BKUseType.School:
            return 21.0
        case BKUseType.ShoppingMall:
            return 21.0
        case BKUseType.SportsHall:
            return 24.0
        case BKUseType.SwimmingPool:
            return 26.0
        }
    }
}