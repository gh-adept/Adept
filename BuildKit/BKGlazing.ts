//
//  BKGlazing.ts
//  Adept
//
//  Created on November 20, 2016 by Animesh.
//  Copyright (c) 2016 Green Hill Products Limited
//  All Rights Reserved.
//

import { BKGlazingPane }      from "./BKGlazingPane"
import { BKGlazingCavity }    from "./BKGlazingCavity"

export class BKGlazing
{
    public height: number
    public width: number
    public rValue: number
    public panes: Array<BKGlazingPane>
    public cavities: Array<BKGlazingCavity>
    
    public constructor(json: any)
    {
        this.height   = json.height
        this.width    = json.width
        this.panes    = this.initPanes(json.panes)
        this.cavities = this.initCavities(json.cavities)
        this.rValue   = this.thermalResistance()
    }

    public initPanes(json: any): Array<BKGlazingPane>
    {
        let panes = []
        for (var pane of json) {
            panes.push(new BKGlazingPane(pane))
        }
        return panes
    }

    public initCavities(json: any): Array<BKGlazingCavity>
    {
        let cavities = []
        for (var cavity of json) {
            cavities.push(new BKGlazingCavity(cavity))
        }
        return cavities
    }

    public thermalResistance(): number
    {
        let rValue = 0
        for (let pane of this.panes) {
            rValue += pane.rValue
        }
        for (let cavity of this.cavities) {
            rValue += cavity.rValue
        }

        return rValue
    }

    public area(): number
    {
        return this.height * this.width
    }
}