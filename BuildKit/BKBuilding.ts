//
//  BKBuilding.ts
//  Adept
//
//  Created on November 22, 2016 by Animesh.
//  Copyright (c) 2016 Green Hill Products Limited
//  All Rights Reserved.
//

import { AdeptError }       from "../Helpers/AdeptError"
import { AdeptErrorType }   from "../Helpers/AdeptError"
import { BKUse }            from "./BKUse"
import { BKLocale }         from "./BKLocale"
import { BKWall }           from "./BKWall"
import { BKRoof }           from "./BKRoof"
import { BKFloor }          from "./BKFloor"
import { BKControlSystem }  from "./BKControlSystem"
import { BKControlSystemType } from "./BKControlSystem"
import { JSONPurpose }      from "../Helpers/JSONPurpose"

export class BKBuilding 
{
    public _id      : string
    public _rev     : string
    /** ID of the User account to which this building belongs */
    public parent   : string
    /** IDs of User who have view-only access to the building */
    public sharedWith: Array<string>

    public epcNumber: string
    public name     : string
    public yearBuilt: number

    // --- Dimensions --- //
    public length  : number
    public width   : number
    public storeys : number
    public storeyHeight: number

    // --- Structural information --- //
    public locale  : BKLocale
    public walls   : Array<BKWall>
    public roof    : BKRoof
    public floor   : BKFloor

    // --- Building use information --- //
    public use     : BKUse
    public controlSystems : Array<BKControlSystem>
    // public projections: Array<EKProjection>

    // ----------------------------------------------------------------------------------------------------------------- //

    public constructor(purpose: JSONPurpose, json: any, parent: string)
    {
        this.parent       = parent
        this.sharedWith   = json.sharedWith
        this.epcNumber    = json.epcNumber
        this.name         = json.name
        this.yearBuilt    = json.yearBuilt
        this.length       = json.length
        this.width        = json.width
        this.storeys      = json.storeys
        this.storeyHeight = json.storeyHeight
        this.locale       = new BKLocale(json.locale)        
        
        switch(purpose) {
        case JSONPurpose.InitFromClient:
            break
        case JSONPurpose.InitFromDb:
        case JSONPurpose.Projections:
        case JSONPurpose.Update:
            this._id      = json._id
            this._rev     = json._rev
            this.walls    = json.walls ? this.initWalls(json.walls) : null
            this.controlSystems = json.controlSystems ? this.initSystems(json.controlSystems) : null
            this.roof     = json.roof ? new BKRoof(json.roof) : null
            this.floor    = json.floor ? new BKFloor(json.floor) : null
            this.use      = json.use ? new BKUse(json.use) : null
            break
        }
    }

    public initSystems(json: any): Array<BKControlSystem>
    {
        let allSystems = []
        for (var system of json) {
            allSystems.push(new BKControlSystem(system))
        }
        return allSystems
    }

    public initWalls(json: any): Array<BKWall>
    {
        let walls = []
        for (var wall of json) {
            walls.push(new BKWall(wall))
        }
        return walls
    }

    public volume(): number {
        return this.length * this.width * (this.storeys * this.storeyHeight)
    }

    public heatLossCoefficient(): number
    {
        let sigmaAU = 0
        for (let wall of this.walls) {
            sigmaAU += wall.area(this) * (1 / wall.rValue)
            for (let window of wall.windows) {
                sigmaAU += window.area() * (1 / window.rValue)
            }
            for (let door of wall.doors) {
                sigmaAU += door.area() * (1 / door.rValue)
            }
        }
        sigmaAU += this.roof.area(this) * (1 / this.roof.rValue)
        sigmaAU += this.floor.area(this) * (1 / this.floor.rValue)

        let buildingVolume = this.length * this.width * this.storeyHeight * this.storeys
        let coefficient = (sigmaAU + (0.33 * this.use.ach * buildingVolume)) / 1000
        return coefficient
    }

    public heatGains(): number {
        return this.use.occupancy.totalHeatGeneration
    }

    public baseTemperatureHeating(): number {
        return this.use.minTemp - (this.heatGains() / this.heatLossCoefficient())
    }

    public baseTemperatureCooling(): number {
        return this.use.maxTemp - (this.heatGains() / this.heatLossCoefficient())
    }

    public EnergyConsumption(degreeDays: number): number {
        return (24 * this.heatLossCoefficient() * degreeDays) / 0.75
    }
}