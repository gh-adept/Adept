//
//  BKFloorMaterial.ts
//  Adept
//
//  Created on November 20, 2016 by Animesh.
//  Copyright (c) 2016 Green Hill Products Limited.
//  All Rights Reserved.
//

export enum BKFloorMaterialType {
    Concrete,
    MetalTraySteel,
    Screed,
    TimberHardwood,
    TimberSoftwood
}

export class BKFloorMaterial
{
    public type: string
    public conductivity: number

    public constructor(type: BKFloorMaterialType)
    {
        this.type = BKFloorMaterial.ListTypes()[type]
        this.conductivity = BKFloorMaterial.ConductivityOf(type)
    }

    public static ListTypes(): Array<string>
    {
        return [
            "Cast Concrete",
            "Metal Tray - Steel",
            "Screed",
            "Timber - Hardwood",
            "Timber - Softwood"
        ]
    }

    public static From(type: string): BKFloorMaterialType
    {
        switch(type.toLowerCase()) {
        case "cast concrete":
            return BKFloorMaterialType.Concrete
        case "metal tray - steel":
            return BKFloorMaterialType.MetalTraySteel
        case "screed":
            return BKFloorMaterialType.Screed
        case "timber - hardwood":
            return BKFloorMaterialType.TimberHardwood
        case "timber - softwood":
            return BKFloorMaterialType.TimberSoftwood
        }
    }

    public static ConductivityOf(type: BKFloorMaterialType): number
    {
        switch(type) {
        case BKFloorMaterialType.Concrete:
            return 1.35
        case BKFloorMaterialType.MetalTraySteel:
            return 50.0
        case BKFloorMaterialType.Screed:
            return 0.41
        case BKFloorMaterialType.TimberHardwood:
            return 0.18
        case BKFloorMaterialType.TimberSoftwood:
            return 0.13
        }
    }
}