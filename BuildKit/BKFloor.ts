//
//  BKFloor.ts
//  Adept
//
//  Created on November 20, 2016 by Animesh.
//  Copyright (c) 2016 Green Hill Products Limited
//  All Rights Reserved.
//

import { BKFloorMaterial }  from "./BKFloorMaterial"
import { BKInsulation }     from "./BKInsulation"
import { BKBuilding }       from "./BKBuilding"

export enum BKFloorType {
    Solid,
    Suspended
}

export class BKFloor
{
    public type         : string
    public material     : string
    public thickness    : number
    public insulation   : BKInsulation
    public voidDepth    : number
    public rValue       : number

    public constructor(json: any)
    {
        this.type       = json.type
        this.thickness  = json.thickness

        let insulation  = new BKInsulation(json.insulation)
        this.insulation = insulation

        this.material   = json.material
        let floorMaterial = new BKFloorMaterial(BKFloorMaterial.From(json.material))
  
        this.rValue     = this.thermalResistance(floorMaterial, insulation)
    }

    public area(parent: BKBuilding): number
    {
        return parent.length * parent.width
    }

    public thermalResistance(material: BKFloorMaterial, insulation: BKInsulation)
    {
        let rValueFloor = (this.thickness / 1000) / material.conductivity
        return rValueFloor + insulation.rValue
    }

    public static ListTypes(): Array<string> 
    {
        return [
            "Solid",
            "Suspended"
        ]
    }

    public static From(type: string): BKFloorType
    {
        switch(type.toLowerCase()) {
        case "solid":
            return BKFloorType.Solid
        case "suspended":
            return BKFloorType.Suspended
        }
    }
}