//
//  BKFuel.ts
//  Adept
//  
//  Created on November 19, 2016 by Animesh.
//  Copyright (c) 2016 Green Hill Products Limited
//  All Rights Reserved.
//

export enum Type {
    NaturalGas,
    Oil,
    Coal,
    GridElectricity,
    RenewableElectricity,
    LPG, 
    Biogas
}

export function ListTypes(): Array<string>
{
    return [
        "Natural Gas",
        "Oil",
        "Coal",
        "Grid Electricity",
        "Renewable Electricity",
        "LPG",
        "Biogas"
    ]
}

export function FuelFrom(type: string): Type
{
    switch(type.toLowerCase()) {
    case "natural gas":
        return Type.NaturalGas
    case "oil":
        return Type.Oil
    case "coal":
        return Type.Coal
    case "grid electricity":
        return Type.GridElectricity
    case "renewable electricity":
        return Type.RenewableElectricity
    case "lpg":
        return Type.LPG
    case "biogas":
        return Type.Biogas
    }
}

export function CarbonFactorFor(fuel: Type): number 
{
    switch(fuel) {
    case Type.NaturalGas:
        return 0.194
    case Type.Oil:
        return 0.265
    case Type.Coal:
        return 0.291
    case Type.GridElectricity:
        return 0.422
    }
}