//
//  BKRoof.ts
//  Adept
//
//  Created on November 20, 2016 by Animesh.
//  Copyright (c) 2016 Green Hill Products Limited
//  All Rights Reserved.
//

import { BKBuilding }   from "./BKBuilding"
import { BKInsulation } from "./BKInsulation"

export class BKRoof
{
    public insulation   : BKInsulation
    public rValue       : number

    public constructor(json: any)
    {
        let insulation  = new BKInsulation(json.insulation) 
        this.insulation = insulation
        this.rValue     = this.thermalResistance(insulation)
    }

    public thermalResistance(insulation: BKInsulation)
    {
        return insulation.rValue
    }

    public area(parent: BKBuilding): number
    {
        return parent.length * parent.width
    }
}