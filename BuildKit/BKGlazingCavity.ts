//
//  BKGlazingCavity.ts
//  Adept
// 
//  Created on November 20, 2016 by Animesh.
//  Copyright (c) 2016 Green Hill Products Limited
//  All Rights Reserved.
//

import { BKGlazingCavityFill }   from "./BKGlazingCavityFill"
import { BKGlazingCavitySpacer } from "./BKGlazingCavitySpacer"

export class BKGlazingCavity
{
    public width: number
    public fill: string
    public spacerMaterial: string
    public rValue: number

    public constructor(json: any)
    {
        this.width  = json.width
        
        this.fill   = json.fill
        let gasFill = new BKGlazingCavityFill(BKGlazingCavityFill.FromFill(json.fill))
        
        this.spacerMaterial = json.spacerMaterial
        let spacer  = new BKGlazingCavitySpacer(json.spacerMaterial)

        this.rValue = this.thermalResistance(gasFill, spacer)
    }

    public thermalResistance(cavityFill: BKGlazingCavityFill, spacer: BKGlazingCavitySpacer): number
    {
        let rValueCavity = (this.width / 1000) / cavityFill.conductivity
        let rValueSpacer = (this.width / 1000) / spacer.conductivity
        return rValueCavity + rValueSpacer
    }
}