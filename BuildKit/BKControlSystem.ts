/**
 *  BKControlSystem.ts
 *  Adept.ClimateKit
 * 
 *  Buildings often employ several control systems - heating, cooling, 
 *  flooding etc. It's important to model the fuel used by such systems (if any)
 *  and their efficiency.
 *  
 *  @created    January 15, 2017
 *  @author     Animesh. hey@animesh.xyz
 *  @copyright  Copyright (c) 2016 Green Hill Products Limited
 *              All Rights Reserved
 * 
 */

export enum BKControlSystemType {
    Cooling,
    Flooding,
    Heating
}

export class BKControlSystem
{
    public type: string
    public fuel: string
    public efficiency: number

    public constructor(json: any)
    {
        this.type = json.type
        this.fuel = json.fuel
        this.efficiency = json.efficiency
    }

    public static ListTypes(): Array<string>
    {
        return [
            "Cooling",
            "Flooding",
            "Heating"
        ]
    }
}