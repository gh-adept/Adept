//
//  BKWallCavity.ts
//  Adept
//
//  Created on November 20, 2016 by Animesh.
//  Copyright (c) 2016 Green Hill Products Limited
//  All Rights Reserved.
//

import { BKInsulation } from "./BKInsulation"

export enum BKWallCavityType {
    None,
    Empty,
    PartFilled,
    FullFilled
}

export class BKWallCavity
{
    public type         : string
    public width        : number
    public insulation   : BKInsulation
    public rValue       : number

    public constructor(json: any) 
    {
        let cavityType  = BKWallCavity.From(json.type)  
        this.type       = json.type
        this.width      = json.width
        let insulation  = new BKInsulation(json.insulation)
        this.insulation = insulation
        this.rValue     = this.thermalResistance(cavityType)
    }

    public thermalResistance(type: BKWallCavityType): number
    {
        switch(type) {
        case BKWallCavityType.None:
            return null
        case BKWallCavityType.Empty:
            // 0.024 is the thermal conductivity of air
            return (this.width / 1000) / 0.024
        case BKWallCavityType.PartFilled:
        case BKWallCavityType.FullFilled:
            let airWidth = this.width - this.insulation.thickness
            return ((airWidth / 1000) / 0.024) + this.insulation.rValue 
        }
    }

    public static ListTypes(): Array<string> {
        return [
            "None",
            "Empty",
            "Partially Filled",
            "Fully Filled"
        ]
    }

    public static From(type: string): BKWallCavityType
    {
        switch(type.toLowerCase()) {
        case "none":
            return BKWallCavityType.None
        case "empty":
            return BKWallCavityType.Empty
        case "partially filled":
            return BKWallCavityType.PartFilled
        case "fully filled":
            return BKWallCavityType.FullFilled
        }
    }
}