/**
 * 
 *  BKOccupantActivity.ts
 *  Adept
 * 
 *  Metabolic heat production is one of the many contributing
 *  factors to internal heat gains of a building. It is largely
 *  largely dependent on occupants' activities and is measured
 *  in met.
 * 
 *  A met is approximately the metabolic rate of a person seated
 *  at rest. 1 met = 58.2 W/m2. Average body surface area for adults
 *  is about 1.8 m2, therefore 1 met is equivalent to approximately
 *  100 W of total heat emission.
 *  
 *  @created    December 10, 2016
 *  @author     Animesh. ash@mishra.io
 *  @copyright  Copyright (c) 2016 Green Hill Products Limited
 *              All Rights Reserved.
 */

/** 1 met = 58.2 W/m2 */
const WattsPerSquareMetresInOneMet = 58.2

export enum BKOccupantActivityType
{
    Sleeping,
    Reclining,
    Seated,
    Standing,
    Walking,    // 0.9 m/s
    Running,    // 1.8 m/s
    Reading,
    Writing,
    Typing,
    FilingSeated,
    FilingStanding,
    Packing,
    Cooking,
    HouseCleaning,
    MachineSawing,
    LightMachineWork,
    HeavyMachineWork,
    HandlingHeavyWeight,    // 50kg bags
    Dancing,
    Exercise,
    Tennis,
    Basketball,
    Wrestling
}

export class BKOccupantActivity 
{
    public type: string

    /** Heat generation per unit area of body surface, W/m2 */
    public heatGeneration: number

    public constructor(json: any)
    {
        this.type = json.type
        // Convert string value into a valid BKOccupantActivity
        let activityType = BKOccupantActivity.From(json.type)
        this.heatGeneration = BKOccupantActivity.MetabolicRateForActivity(activityType) * WattsPerSquareMetresInOneMet
    }

    public static ListTypes(): Array<string>
    {
        return [
            "Sleeping",
            "Reclining",
            "Seated",
            "Standing",
            "Walking (0.9 m/s)",
            "Running (1.8 m/s)",
            "Reading",
            "Writing",
            "Typing",
            "Filing, seated",
            "Filing, standing",
            "Packing",
            "Cooking",
            "House Cleaning",
            "Machine Sawing",
            "Light Machine Work",
            "Heavy Machine Work",
            "Handing Heavy Weights (50kg)",
            "Dancing",
            "Exercise",
            "Tennis",
            "Basketball",
            "Wrestling"
        ]
    }

    public static From(type: string): BKOccupantActivityType
    {
        switch(type.toLowerCase()) {
        case "sleeping":
            return BKOccupantActivityType.Sleeping
        case "reclining":
            return BKOccupantActivityType.Reclining
        case "seated":
            return BKOccupantActivityType.Seated
        case "standing":
            return BKOccupantActivityType.Standing
        case "walking, (0.9 m/s)":
            return BKOccupantActivityType.Walking
        case "running, (1.8 m/s)":
            return BKOccupantActivityType.Running
        case "reading":
            return BKOccupantActivityType.Reading
        case "writing":
            return BKOccupantActivityType.Writing
        case "typing":
            return BKOccupantActivityType.Typing
        case "filing, seated":
            return BKOccupantActivityType.FilingSeated
        case "filing, standing":
            return BKOccupantActivityType.FilingStanding
        case "packing":
            return BKOccupantActivityType.Packing
        case "cooking":
            return BKOccupantActivityType.Cooking
        case "house cleaning":
            return BKOccupantActivityType.HouseCleaning
        case "machine sawing":
            return BKOccupantActivityType.MachineSawing
        case "light machine work":
            return BKOccupantActivityType.LightMachineWork
        case "heavy machine work":
            return BKOccupantActivityType.HeavyMachineWork
        case "handling heavy weights (50kg)":
            return BKOccupantActivityType.HandlingHeavyWeight
        case "dancing":
            return BKOccupantActivityType.Dancing
        case "exercise":
            return BKOccupantActivityType.Exercise
        case "tennis":
            return BKOccupantActivityType.Tennis
        case "basketball":
            return BKOccupantActivityType.Basketball
        case "wrestling":
            return BKOccupantActivityType.Wrestling
        }
    }

    /** Metabolic rate here is reported in Met. 1 Met = 58.2 Watts per square metre. */
    private static MetabolicRateForActivity(type: BKOccupantActivityType)
    {
        switch (type) {
        case BKOccupantActivityType.Sleeping:
            return 0.7
        case BKOccupantActivityType.Reclining:
            return 0.8
        case BKOccupantActivityType.Seated:
            return 1.0
        case BKOccupantActivityType.Standing:
            return 1.2
        case BKOccupantActivityType.Walking:
            return 2.0
        case BKOccupantActivityType.Running:
            return 3.8
        case BKOccupantActivityType.Reading:
            return 1.0
        case BKOccupantActivityType.Writing:
            return 1.0
        case BKOccupantActivityType.Typing:
            return 1.1
        case BKOccupantActivityType.FilingSeated:
            return 1.2
        case BKOccupantActivityType.FilingStanding:
            return 1.4
        case BKOccupantActivityType.Packing:
            return 2.1
        case BKOccupantActivityType.Cooking:
            return 2.3
        case BKOccupantActivityType.HouseCleaning:
            return 3.4
        case BKOccupantActivityType.MachineSawing:
            return 1.8
        case BKOccupantActivityType.LightMachineWork:
            return 2.0
        case BKOccupantActivityType.HeavyMachineWork:
            return 3.0
        case BKOccupantActivityType.HandlingHeavyWeight:
            return 4.0
        case BKOccupantActivityType.Dancing:
            return 4.4
        case BKOccupantActivityType.Exercise:
            return 4.0
        case BKOccupantActivityType.Tennis:
            return 4.0
        case BKOccupantActivityType.Basketball:
            return 7.6
        case BKOccupantActivityType.Wrestling:
            return 8.7
        }
    }
}