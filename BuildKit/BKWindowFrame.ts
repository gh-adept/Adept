//
//  BKWindowFrame.ts
//  Adept
//
//  Created on November 20, 2016 by Animesh.
//  Copyright (c) 2016 Green Hill Products Limited
//  All Rights Reserved.
//

export enum BKWindowFrameMaterial {
    Aluminium,
    Hardwood,
    PVC,
    Softwood,
    Steel
}

export class BKWindowFrame
{
    public material  : string
    public width     : number
    public thickness : number
    public rValue    : number

    public constructor(json: any)
    {
        this.width     = json.width
        this.thickness = json.thickness
        this.material  = json.material

        let frameMaterial = BKWindowFrame.From(json.material)
        this.rValue       = this.thermalResistance(frameMaterial)
    }

    public thermalResistance(material: BKWindowFrameMaterial): number
    {
        return (this.thickness / 1000) / BKWindowFrame.ConductivityOf(material)
    }

    public area(glazingHeight: number, glazingWidth: number): number
    {
        return (2 * this.width) * ((2 * this.width) + glazingHeight + glazingWidth)
    }

    public static ListMaterials(): Array<string> {
        return [
            "Aluminium",
            "Hardwood",
            "PVC",
            "Softwood",
            "Steel"
        ]
    }

    public static From(material: string): BKWindowFrameMaterial
    {
        switch(material.toLowerCase()) {
        case "aluminium":
            return BKWindowFrameMaterial.Aluminium
        case "hardwood":
            return BKWindowFrameMaterial.Hardwood
        case "pvc":
            return BKWindowFrameMaterial.PVC
        case "softwood":
            return BKWindowFrameMaterial.Softwood
        case "steel":
            return BKWindowFrameMaterial.Steel
        }
    }

    public static ConductivityOf(material: BKWindowFrameMaterial): number
    {
        switch(material) {
        case BKWindowFrameMaterial.Aluminium:
            return 45.00
        case BKWindowFrameMaterial.Hardwood:
            return 0.16
        case BKWindowFrameMaterial.PVC:
            return 0.16
        case BKWindowFrameMaterial.Softwood:
            return 0.14
        case BKWindowFrameMaterial.Steel:
            return 45.00
        }
    }
}