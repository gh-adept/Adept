//
//  BKGlazingPane.ts
//  Adept
//
//  Created on November 20, 2016 by Animesh.
//  Copyright (c) 2016 Green Hill Products Limited
//  All Rights Reserved.
//

export enum BKGlazingPaneCoating {
    Unknown
}

export class BKGlazingPane
{
    public coating: string
    public thickness: number

    /** 1/Ug value rated by the manufactured of the glass pane */
    public rValue: number

    public constructor(json: any) 
    {
        this.thickness = json.thickness
        this.rValue    = json.rValue
        this.coating   = json.coating
    }

    public thermalResistance(uValue: number): number
    {
        return 1 / uValue
    }

    public static ListCoatingTypes(): Array<string> {
        return [
            "Unknown"
        ]
    }

    public static From(coating: string): BKGlazingPaneCoating
    {
        switch(coating.toLowerCase()) {
        case "unknown":
            return BKGlazingPaneCoating.Unknown
        }
    }
}