/**
 *  OrgAPI.ts
 *  Adept
 *  
 *  @requires   express, jsonwebtoken
 *  @requires   AdeptError, AdeptErrorType, Organisation, Router
 *  
 *  @created    December 8, 2016
 *  @author     Animesh. ash@mishra.io
 *  @copyright  Copyright (c) 2016 Green Hill Products Limited
 *              All Rights Reserved.
 * 
 */

import * as Express      from "express"
import * as JWT          from "jsonwebtoken"

import { AdeptError }    from "../Helpers/AdeptError"
import { AdeptErrorType} from "../Helpers/AdeptError"
import { Organisation }  from "./Organisation"
import { Router }        from "../Controllers/Router"

/**
 *  Decodes the access token provided by the client to extract 
 *  user ID.
 *  
 *  @param {string} token Access token provided by the client
 *  @returns {string} GUID of the user identified by the token
 */
function UserIDFrom(token: string): string 
{
    let decoded = JWT.decode(token, { complete: true })
    return decoded.payload._id
}

/**
 *  @route  /orgs
 *  @http   POST
 */
export function Create(request: Express.Request, response: Express.Response, next: Express.NextFunction)
{
    let token  = request.headers["x-access-token"]
    let userID = UserIDFrom(token) 

    let name    = request.body.name
    let address = request.body.address
    let email   = request.body.email
    let website = request.body.website
    let org     = new Organisation(name, address, email, website, userID)

    // Let's make sure this organisation doesn't exist already
    let predicate = {
        "selector": {
            "name": org.name
        }
    }

    Router.dataService.add("organisations", org, predicate, (data, error: AdeptError) => {
        if (error) {
            error.message = Router.Sanitize(error.message)
            delete error.message._id
            response.status(500).json(error)
        }
        else {
            if (data.ok) {
                org._id = data.id
                response.status(200).json(Router.Sanitize(org))
            }
            else {
                response.status(500).json(new AdeptError(AdeptErrorType.WTF, Router.Sanitize(data)))
            }
        }
    })
}

/**
 *  @route  /orgs/:orgID
 *  @http   GET
 */
export function Get(request: Express.Request, response: Express.Response, next: Express.NextFunction)
{
    let token  = request.headers["x-access-token"]
    let userID = UserIDFrom(token)
    let orgID  = request.params.orgID

    Router.dataService.get("organisations", orgID, (data, error) => {
        if (error) {
            error.message = Router.Sanitize(error.message)
            delete error.message._id
            response.status(500).json(error)
        }
        else {
            let org     = new Organisation(data.name, data.address, data.email, data.website)
            org._id     = data._id
            org._rev    = data._rev
            org.members = data.members
            org.admins  = data.admins

            if (org.canBeAccessedBy(userID)) {
                response.status(200).json(Router.Sanitize(org))
            }
            else {
                response.status(403).json(new AdeptError(AdeptErrorType.CouldntGet, "You aren't a member of this organisation."))
            }
        }
    })
}

/**
 *  @route  /orgs/:orgID
 *  @http   PUT
 */
export function Update(request: Express.Request, response: Express.Response, next: Express.NextFunction)
{
    let token   = request.headers["x-access-token"]
    let userID  = UserIDFrom(token)
    let orgID   = request.params.orgID

    Router.dataService.get("organisations", orgID, (data, error: AdeptError) => {
        if (error) {
            error.message = Router.Sanitize(error.message)
            delete error.message._id
            response.status(500).json(error)
        }
        else {
            let org     = new Organisation(data.name, data.address, data.email, data.website)
            org.members = data.members
            org.admins  = data.admins

            if (org.canbeModifiedBy(userID)) {
                org         = request.body
                org.members = request.body.members ? request.body.members : data.members
                org.admins  = request.body.admins  ? request.body.admins  : data.admins
                org._id     = data._id
                org._rev    = data._rev
                
                Router.dataService.add("organisations", org, null, (data, error: AdeptError) => {
                    if (error) {
                        error.message = Router.Sanitize(error.message)
                        delete error.message._id
                        response.status(500).json(error)
                    }
                    else {
                        response.status(200).json(Router.Sanitize(data))
                    }
                })
            }
            else {
                response.status(403).json(new AdeptError(AdeptErrorType.CouldntEdit, "You don't have editing privileges."))
            }
        }
    }) 
}

/**
 *  @route  /orgs/:orgID
 *  @http   DELETE
 */
export function Delete(request: Express.Request, response: Express.Response, next: Express.NextFunction)
{
    let token   = request.headers["x-access-token"]
    let userID  = UserIDFrom(token)
    let orgID   = request.params.orgID

    Router.dataService.get("organisations", orgID, (data, error: AdeptError) => {
        if (error) {
            error.message = Router.Sanitize(error.message)
            delete error.message._id
            response.status(500).json(error)
        }
        else {
            let org     = new Organisation(data.name, data.address, data.email, data.website)
            org.members = data.members
            org.admins  = data.admins

            if (org.canbeModifiedBy(userID)) {
                Router.dataService.delete("organisations", orgID, (error: AdeptError) => {
                    if (error) {
                        error.message = Router.Sanitize(error.message)
                        delete error.message._id
                        response.status(500).json(error)
                    }
                    else {
                        response.status(200).json(`Successfully deleted organisation with ID ${orgID}.`)
                    }
                })
            }
            else {
                response.status(403).json(new AdeptError(AdeptErrorType.CouldntDelete, "You don't have editing privileges."))
            }
        }
    })
}