//
//  Organisation.ts
//  Adept
//
//  Created on December 5, 2016 by Animesh.
//  Copyright (c) Green Hill Products Limited
//  All Rights Reserved.
//

export class Organisation
{
    public _id      : string
    public _rev     : string
    public name     : string
    public address  : string
    public email    : string
    public website  : string
    public members  : Array<string>
    public admins   : Array<string>

    public constructor(name: string, address: string, email: string, website: string, userID?: string)
    {
        this.name       = name
        this.address    = address
        this.email      = email
        this.website    = website
        if (userID) {
            this.members    = [ userID ]
            this.admins     = [ userID ]
        }
    }

    public canBeAccessedBy(userID: string): boolean
    {
        for (let member of this.members) {
            if (member == userID) {
                return true
            }
        }

        return false
    }

    public canbeModifiedBy(userID: string): boolean
    {
        for (let member of this.admins) {
            if (member == userID) {
                return true
            }
        }

        return false
    }

    public addAdmins(IDs: Array<string>)
    {
        for (let ID of IDs) {
            this.admins.push(ID)
        }
        this.add(IDs)
    }

    public add(newUserIDs: Array<string>)
    {
        for (let userID of newUserIDs) {
            this.members.push(userID)
        }
    }

    public remove(userIDs: Array<string>)
    {
        for (let userID of userIDs) {
            this.members = this.members.map( value => {
                if (value != userID) {
                    return value
                }
            }).filter(value => !!value)
        }
    }
}