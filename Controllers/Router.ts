/**
 *  Router.ts
 *  Adept
 *  
 *  @requires   body-parser, cors, express, jsonwebtoken, morgan, path
 *  @requires   AppInfo, BuildingAPI, ClimateAPI, DataService, Options, UserAPI
 * 
 *  @created    November 19, 2016
 *  @author     Animesh. ash@mishra.io
 *  @copyright  Copyright (c) 2016 Green Hill Products Limited.
 *              All Rights Reserved.
 */

import * as BodyParser      from "body-parser"
import * as CORS            from "cors"
import * as Express         from "express"
import * as JWT             from "jsonwebtoken"
import * as Logger          from "morgan"
import * as Path            from "path"

import { AppInfo }          from "./AppInfo"
import { DataService }      from "./DataService"
import * as BuildingAPI     from "../BuildKit/BuildingAPI"
import * as OptionsAPI      from "../Helpers/OptionsAPI"
import * as OrgAPI          from "../Organisation/OrgAPI"
import * as ProjectionAPI   from "../Helpers/ProjectionAPI"
import * as UserAPI         from "../User/UserAPI"

export class Router
{
    /** Deployment information */
    public static appInfo: AppInfo

    /** Controller for managing database I/O */
    public static dataService: DataService

    /**
     *  Configures routes for the application. To avoid repetition, or
     *  worse routing conflict, routes and middlewares mustn't be 
     *  defined elsewhere.
     * 
     *  @constructor
     *  @param  {Express} app The Express application object
     *  @param  {AppInfo} details Deployment details for the app
     *  @param  {DataService} dataService Handles all database I/O operation across the app
     */
    public static Configure(app: Express, details: AppInfo, dataService: DataService) 
    {
        Router.appInfo      = details
        Router.dataService  = dataService

        // Middleware
        app.use(Logger("dev"))
        app.use(BodyParser.json())
        app.use(BodyParser.urlencoded({ extended: true }))
        
        // Enable CORS
        app.use(CORS({ 
            credentials: true,
            exposedHeaders: ["X-Access-Token"]
        }))

        // Registration and login
        app.post("/signup", UserAPI.Signup)
        app.post("/authenticate", UserAPI.Login)

        // Options
        app.get("/options/building", OptionsAPI.Building)

        // Token and version check
        app.use("/api/*", Router.tokenCheck)
        app.use("/api/:version/*", Router.versionCheck)
        app.use("/api/:version/users/:userID/*", Router.userVerification)

        // User profile management
        app.get("/api/:version/users/:userID", UserAPI.Get)
        app.put("/api/:version/users/:userID", UserAPI.Update)
        app.delete("/api/:version/users/:userID", UserAPI.Delete)

        // Building management
        app.post("/api/:version/buildings", BuildingAPI.Create)
        app.get("/api/:version/buildings", BuildingAPI.GetAll)
        app.get("/api/:version/buildings/:buildingID", BuildingAPI.Get)
        app.put("/api/:version/buildings/:buildingID", BuildingAPI.Update)
        app.delete("/api/:version/buildings/:buildingID", BuildingAPI.Delete)

        // Organisation management
        app.post("/api/:version/orgs", OrgAPI.Create)
        // app.get("/api/:version/orgs", OrgAPI.GetAll)
        app.get("/api/:version/orgs/:orgID", OrgAPI.Get)
        app.put("/api/:version/orgs/:orgID", OrgAPI.Update)
        app.delete("/api/:version/orgs/:orgID", OrgAPI.Delete)

        // Projections API
        app.post("/api/:version/projections/energy", ProjectionAPI.EnergyConsumption)
        // app.get("/api/:version/buildings/:buildingID/interventions/:intID", BuildingAPI.getIntervention)
        // app.put("/api/:version/buildings/:buildingID/interventions/:intID", BuildingAPI.updateIntervention)
        // app.delete("/api/:version/buildings/:buildingID/interventions/:intID", BuildingAPI.deleteIntervention)
    }

    /**
     *  Removes sensitive information such as password, salt, 
     *  database revision ID etc. This ensure privileged data 
     *  is not leaked by the server.
     *  
     *  @public 
     *  @static
     *  @param   {any} information Object to be sanitised
     *  @returns {any} Sanitised object
     */
    public static Sanitize(information: any): any
    {
        // Remove error stack
        delete information.stack
        return information
    }

    /**
     *  Verifies token provided with a request. If the verification is successful,
     *  pushes the request further along the request handling pipeline. On failure,
     *  request is ended and an error sent in the response.
     *  
     *  @private 
     *  @static 
     */
    private static tokenCheck(request: Express.Request, response: Express.Response, next: Express.NextFunction)
    {
        let token = request.headers["x-access-token"]
        if (token) {
            try {
                JWT.verify(token, AppInfo.SecretKey())
                next()
            }
            catch (error) {
                console.log(error)
                delete error.stack
                response.set("Content-Type", "application/json")
                response.status(403).json(error)
            }
        }
        else {
            response.status(403).json("Please provide a token as HTTP header X-Access-Token to authenticate your request.")
        }
    }

    /**
     *  Checks if the version targeted by an API call is available. If not,
     *  then an error is sent back to client explaining the issue.
     *  
     *  @private 
     *  @static 
     */
    private static versionCheck(request: Express.Request, response: Express.Response, next: Express.NextFunction) 
    {
        if (Router.appInfo.DoesSupport(request.params.version)) {
            next()
        }
        else {
            let message = "Using deprecated version. Please try" + Router.appInfo.version + " instead."
            let error = {
                type: "Deprecated Version",
                reason: message
            }
            response.status(403).json(error)
        }
    }

    /**
     *  Verifies whether the request user ID matches the one for which the
     *  access token was generated. If not, returns a token mismatch error. 
     *  This helps prevent someone with a valid token from accessing data 
     *  of users other than the one for which the token was issued.
     *  
     *  @private 
     *  @static 
     */
    private static userVerification(request: Express.Request, response: Express.Response, next: Express.NextFunction)
    {
        // Check whether userID matches the one in the token
        let token = request.headers["x-access-token"]
        let decoded = JWT.decode(token, { complete: true })
        
        if (decoded.payload._id != request.params.userID) {
            response.status(403).json("The access token doesn't match the user account you are trying to access.")
        }
        else {
            next()
        }
    }
}