/**
 *  AppInfo.ts
 *  Adept
 * 
 *  Defines a class `AppInfo` that models deployment information about the
 *  application. An `AppInfo` instance is used to configure the `Router`.
 *  `AppInfo` class also provides the secret key used to sign web tokens 
 *  issued by the application.
 *  
 *  @requires   fs, path
 * 
 *  @created    November 19, 2016
 *  @author     Animesh. ash@mishra.io
 *  @copyright  Copyright (c) 2016 Green Hill Products Limited.
 *              All Rights Reserved.
 */

import * as Path        from "path"
import * as FileSystem  from "fs"

export class AppInfo
{
    /**  IP address of the server hosting the application */
    public readonly hostname: string

    /** Port at which the server is listening */
    public readonly port: number

    /** Public URL at which the application can be reached */
    public readonly url: string

    /** Name of the application (on Bluemix) */
    public readonly name: string

    /** Latest API version, used to block calls to deprecated/private versions */
    public readonly version: string

    /**
     *  Application name and API version are hardcoded, while hostname, port and
     *  url are set via the parameters passed by Cloud Foundry environment.
     *  
     *  @constructor
     *  @public 
     *  @param {string} hostname IP address of the server
     *  @param {number} port Server's listening port
     *  @param {string} url Public URL at which the application can be reached
     */    
    public constructor(hostname?: string, port?: number, url?: string) 
    {
        this.hostname   = hostname
        this.port       = port
        this.url        = url
        this.name       = "Adept"
        this.version    = "v1"
    }

    /**
     *  Checks whether the given version is supported by the API server.
     *  
     *  @public 
     *  @param {string} version - The version identifier used in request URL.
     *  E.g. "v1" in case of "https://abc.io/api/v1/def"
     *  @returns {boolean} Returns `true` if the API version is supported and live,
     *  otherwise returns `false`. Use `AppInfo.getVersion()` to get the latest API version.
     */
    public DoesSupport(version: string): boolean 
    {
        if (version == this.version) {
            return true
        }
        return false
    }

    /**
     *  Reads a local file containing a secret key that is used for signing
     *  JSON web tokens issued by the application. This local file is named
     *  .privateKey and is not checked into source code, it can be downloaded
     *  from the server hosted in the cloud.
     * 
     *  @public 
     *  @static
     *  @returns {string} The private key used to sign tokens issued by this application
     */
    public static SecretKey(): string
    {
        let path = Path.join(__dirname, "../../.privateKey")
        return FileSystem.readFileSync(path, "utf8")
    }
}