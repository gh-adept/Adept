# Adept

Adept allows users to assess how buildings can be made more sustainable, protected and resilient to impact of climate change
in the most cost-effective manner possible. This website explains how the data is structured in our mathematical models and 
enlists features that the server exposes through a REST API.

## Current release
The stable release at the moment is version `0.1`. 
API reference is here: [Adept API | Documentation](https://siranimesh.github.io/Adept/)

---
Copyright &copy; 2016 Green Hill Products Limited. All Rights Reserved.
