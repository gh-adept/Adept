//
//  User.ts
//  Adept
//
//  Created on November 27, 2016 by Animesh.
//  Copyright (c) 2016 Green Hill Products Limited
//  All Rights Reserved.
//

import * as Crypto        from "crypto"
import { BKBuilding }     from "../BuildKit/BKBuilding"
import { JSONPurpose }    from "../Helpers/JSONPurpose"
import { Organisation }   from "../Organisation/Organisation"

export class User
{
    public _id          : string
    public _rev         : string
    public firstName    : string
    public lastName     : string
    public email        : string
    public password     : string
    public salt         : string

    public constructor(purpose: JSONPurpose, json: any)
    {
        this.firstName      = json.firstName
        this.lastName       = json.lastName
        this.email          = json.email

        switch(purpose) {
        case JSONPurpose.InitFromClient:
            this.password   = this.hash(json.password)
            break
        case JSONPurpose.InitFromDb:
            this._id        = json._id
            this._rev       = json._rev
            this.salt       = json.salt
            this.password   = json.password
            break
        case JSONPurpose.Update:
            this._id        = json._id
            this._rev       = json._rev
            if(json.password) {
                this.password = this.hash(json.password)
            }
            break
        }
    }

    // Hash plain-text password before storing it in database
    public hash(password: string, salter?: string): string
    {
        if (salter == null) {
            let salter = Crypto.randomBytes(128).toString("base64")
            this.salt = salter
        }
        else {
            this.salt = salter
        }

        return Crypto.pbkdf2Sync(password, this.salt, 10000, 512, "sha512").toString("base64")
    }

    public isAuthenticatedUsing(givenPassword: string): boolean 
    {
        return this.password == Crypto.pbkdf2Sync(givenPassword, this.salt, 10000, 512, "sha512").toString("base64")
    }

    /**
     *  Returns a client-ready version of the user object
     */
    public sanitize(): User
    {
        // Copy the object so changes don't affect the original
        let clientVersion = JSON.parse(JSON.stringify(this))
        delete clientVersion.password
        delete clientVersion.salt
        return clientVersion
    }
}