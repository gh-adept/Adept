/**
 *  UserAPI.ts
 *  Adept
 * 
 *  UserAPI acts as a communication broker between the `Router`,
 *  the `DataService` and the `User` model. It provides methods 
 *  for handling `GET`, `PUT` and `DELETE` requests.
 *  
 *  @requires   express, jsonwebtoken
 *  @requires   AdeptError, AppInfo, User, Router 
 * 
 *  @created    November 27, 2016
 *  @author     Animesh. ash@mishra.io
 *  @copyright  Copyright (c) 2016 Green Hill Products Limited.
 *              All Rights Reserved.
 */

import * as Express     from "express"
import * as JWT         from "jsonwebtoken"

import { AdeptError }   from "../Helpers/AdeptError"
import { AppInfo }      from "../Controllers/AppInfo"
import { JSONPurpose }  from "../Helpers/JSONPurpose"
import { User }         from "./User"
import { Router }       from "../Controllers/Router"

/**
 *  Handler for a signup request. If the user email already exists in the 
 *  database, reponds with a `AdeptErrorType.Duplicate` with the existing
 *  object appended to the `error` as the `message` property.
 * 
 *  @route  /signup
 *  @http   POST
 */
export function Signup(request: Express.Request, response: Express.Response, next: Express.NextFunction) 
{
    let user = new User(JSONPurpose.InitFromClient, request.body)

    // Cloudant query to check for duplicate email
    let predicate = {
        "selector": {
            "email": user.email
        }
    }

    // Add to database
    Router.dataService.add("users", user, predicate, (data, error: AdeptError) => {
        if (error == null) {
            if (data.ok) {
                user._id  = data.id
                user._rev = data.rev
                response.status(200).json(user.sanitize())
            }
            else {
                response.status(500).json(data)
            }
        }
        else {
            error.message = Router.Sanitize(error.message)
            response.status(403).json(error)
        }
    })
}

/**
 *  Handles login requests. Upon successful login, returns a signed JSON web token
 *  in the `X-Access-Token` response header.
 *  
 *  @route  /authenticate
 *  @http   POST
 */
export function Login(request: Express.Request, response: Express.Response, next: Express.NextFunction) 
{
    console.log(request.body)
    let email    = request.body.email
    let password = request.body.password

    // DB Query
    let query = {
        "selector": {
            "email": email
        }
    }

    Router.dataService.search("users", query, (queryResult, error) => {
        if (error) {
            response.status(403).json(error)
        }
        else {
            // `data` could be returned as an array of documents, for now
            // a shitty workaround is to pop the last document from the array
            // and work with that. Should be fixed in later versions.
            let dbJSON = queryResult.docs.pop()
            let user = new User(JSONPurpose.InitFromDb, dbJSON)
            if (user.isAuthenticatedUsing(password)) {
                // Generate token
                let token = JWT.sign(user, AppInfo.SecretKey(), { expiresIn: "14 days" })
                response.setHeader("X-Access-Token", token)
                response.status(200).json(user.sanitize())
            }
            else {
                response.status(403).json("Incorrect password.")
            }
        }
    })
}

/**
 *  Handler for getting a User's details.
 *  
 *  @route  /users/:userID
 *  @http   GET
 */
export function Get(request: Express.Request, response: Express.Response, next: Express.NextFunction)
{
    let id = request.params.userID

    Router.dataService.get("users", id, (data, error) => {
        if (error) {
            response.status(404).json(Router.Sanitize(error))
        }
        else {
            let user = new User(JSONPurpose.InitFromDb, data)
            response.status(200).json(user.sanitize())
        }
    })
}

/**
 *  Handler for updating a User instance.
 * 
 *  @route  /users/:userID
 *  @http   PUT
 */
export function Update(request: Express.Request, response: Express.Response, next: Express.NextFunction)
{
    let id = request.params.userID
    let newUser = new User(JSONPurpose.Update, request.body)
    
    // If the request had a new password, the `newUser` object
    // would have been initialised with all properties and is
    // ready for database insertion. 
    // Otherwise we have to pull the object in database, to 
    // add `password` and `salt` properties to our `newUser`.
    // The update operation is not a merge, it's a replace, so
    // newUser object must be fully initialised.

    if(newUser.password) {
        // Write the new object to database
        Router.dataService.add("users", newUser, null, (data, error) => {
            if (error) {
                response.status(500).json(Router.Sanitize(error))
            }
            else {
                if(data.ok) {
                    newUser._rev = data.rev
                    response.status(200).json(newUser.sanitize())
                }
                else {
                    response.status(500).json(data)
                }
            }
        })
    }

    // Get the old user object from database
    else {
        Router.dataService.get("users", id, (data, error) => {
            if (error) {
                response.status(500).json(Router.Sanitize(error))
            }
            else {
                let userInDB     = new User(JSONPurpose.InitFromDb, data)
                newUser.salt     = userInDB.salt
                newUser.password = userInDB.password
                
                // Write the new object to database
                Router.dataService.add("users", newUser, null, (data, error) => {
                    if (error) {
                        response.status(500).json(Router.Sanitize(error))
                    }
                    else {
                        if(data.ok) {
                            newUser._rev = data.rev
                            response.status(200).json(newUser.sanitize())
                        }
                        else {
                            response.status(500).json(data)
                        }
                    }
                })
            }
        })
    }
}

/**
 *  Handler for delete a User account.
 * 
 *  @route  /users/:userID
 *  @http   DELETE
 */
export function Delete(request: Express.Request, response: Express.Response, next: Express.NextFunction)
{
    let id = request.params.userID

    Router.dataService.delete("users", id, (error) => {
        if (error) {
            response.status(500).json(Router.Sanitize(error))
        }
        else {
            response.status(200).end("Successfully deleted user with ID: " + id)
        }
    })
}